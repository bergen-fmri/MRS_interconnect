function [res,varargout]=get_kwarg(kw,def,varargin)
  % GET_KWARG gets the value associated with a keyword argument, from a list of key/value pairs in varargin
  %
  %  res=GET_KWARG(kw,def,varargin) returns varargin{n+1} where n is the (last) index of kw in varargin, or def if kw is not in varargin
  %
  %  res,filtered=GET_KWARG(kw,def,varargin) returns same as above in RES, along with a filtered version of varargin having the pairs associated with kw removed
  % 
  % See also: varargin
  %
  % Alex Craven, University of Bergen

  if length(varargin)==1 && iscell(varargin)
    warning('improper use of get_kwarg: varargin should be key value pairs (pass as varargin{:})');
    varargin=varargin{:}
  end

  ix=find(strcmp(varargin(1:2:end),kw),1,'last');
  if isempty(ix)
    res=def;
  else
    res=varargin{2+(2*(ix-1))};
  end
  if nargout>1
    keep=find(~strcmp(varargin(1:2:end),kw));
    keep=1+kron(keep-1,[2,2])+repmat([0 1],[1,length(keep)]);
    varargout(1)={varargin(keep)};
  end

end
