classdef ordered_map_ARC < containers.Map
  % ordered_map_ARC stores key/value pairs in an ordered structure
  %   This class overloads a standard matlab containers.Map() object, to also maintain ordering of map elements
  %   Objects suport numeric and key-based subscript referencing (subsref), key-based assignment (subsasgn) and vertical concatenation
  %
  % This implementation is EXPERIMENTAL; use with caution.
  %
  % Alex Craven, University of Bergen

  properties 
    order % field order
    % (would prefer protected access, but neither "Access=protected" nor "(GetAccess={?datatable_ARC,?ordered_map_ARC},SetAccess=protected)" behave as expected (or as documented))
  end

  methods % {{{
    function obj=ordered_map_ARC(k,v) % {{{
      obj=obj@containers.Map();
      if nargin==1 && isstruct(k)
        % expand struct
        fn=fieldnames(k);
        for i=1:length(fn)
          s=struct();
          s.type='()';
          s.subs={fn{i}};
          obj.subsasgn(s,k.(fn{i}));
        end
      elseif nargin==2
        % iterate over key/value paired inputs
        for i=1:length(k)
          s=struct();
          s.type='()';
          s.subs={k{i}};
          obj.subsasgn(s,v{i});
        end
      elseif nargin>0
        error('ordered_map_ARC cannot deal with this input type')
      end
    end % }}}

    function varargout=subsref(obj,s) % {{{

      % ref: https://se.mathworks.com/help/matlab/matlab_oop/code-patterns-for-subsref-and-subsasgn-methods.html
      if (strcmp(s(1).type,'()') && isnumeric(s(1).subs{1}) && strcmp(obj.KeyType,'char'))
        s.subs{1}=obj.order{s.subs{1}};
        sref=subsref@containers.Map(obj,s);
      elseif (length(s)==1 && strcmp(s(1).type,'.') && ischar(s.subs) && any(strcmp(s.subs,obj.order))) % allow "." referencing (might be problematic?)
        s.type='()';
        s.subs={s.subs};
        [varargout{1:nargout}]=subsref@containers.Map(obj,s);
      elseif (length(s)==1 && strcmp(s(1).type,'()') && ischar(s(1).subs{1}))
        % looks like standard () indexing... but check if we have a second parameter: this will serve as the default value if key does not exist
        if length(s(1).subs)==2
          if ~obj.isKey(s(1).subs{1})
            [varargout{1:nargout}]=s(1).subs{2};
            return;
          else
            s(1).subs={s(1).subs{1}};
          end
        end
        [varargout{1:nargout}] = subsref@containers.Map(obj,s);
      else
        [varargout{1:nargout}] = builtin('subsref',obj,s);
      end
    end % }}}

    function obj=subsasgn(obj,S,B) % {{{
      if (strcmp(S(1).type,'()') && ~obj.isKey(S.subs{1}))
        % standard indexing; key not already existing; add it to the order
        obj.order{end+1}=S.subs{1};
      end
      obj=subsasgn@containers.Map(obj,S,B);
    end % }}}

    function obj=vertcat(varargin) % {{{
      a=varargin{1};
      neworder=a.order;
      for x=2:length(varargin)
        b=varargin{x};
        % note, for duplicate keys values in b replace those in a.
        for i=1:length(b.order)
          k=b.order{i};
          if ~a.isKey(k)
            neworder{end+1}=k;
          end;
        end
        a=vertcat@containers.Map(a,b);
      end
      obj=a;
      obj.order=neworder;
    end % }}}

    function obj=remove(obj,key) % {{{
      if iscell(key)
        for i=1:length(key)
          obj=obj.remove(key{i});
        end % for
      else
        if obj.isKey(key)
          % remove element from order list
          ix=find(strcmp(key, obj.order));
          obj.order={obj.order{1:ix-1} obj.order{ix+1:end}};
        end % if
        % and remove from map
        remove@containers.Map(obj,key);
      end %if iscell else
    end % function remove }}}

    function obj=show(obj) % {{{
      k=obj.keys();
      v=obj.values();
      for i=1:length(k)
        kk=k{i};
        vv=v{i};
        if isnumeric(vv) && length(vv)==1
          disp(sprintf(' %20s : %d ', kk,vv))
        elseif ischar(vv)
          disp(sprintf(' %20s : %s',kk,vv))
        elseif isstruct(vv) || isa(vv,'containers.Map')
          if isstruct(vv)
            fields=fieldnames(vv);
            disp(sprintf(' %20s : struct',kk));
          else
            fields=keys(vv);
            disp(sprintf(' %20s : nested map',kk));
          end
          for j=1:length(fields)
            if isstruct(vv)
              vvv=formatting.tostring(vv.(fields{j}),100);
            else
              try
                vvv=vv.values({fields{j}});
                vvv=vvv{1};
                vvv=formatting.tostring(vvv,100);
              catch
                vvv='...';
              end
            end
            disp(sprintf('  %20s.%-10s : %-50s',kk,fields{j},vvv));
          end % for
        else
          vvv=formatting.tostring(vvv,100);
          disp(sprintf(' %20s : %-50s',kk,vvv));
          %disp(sprintf('----- %s -----',kk));
          %vv
        end
      end;
    end % show }}}

  end % methods }}}
end
