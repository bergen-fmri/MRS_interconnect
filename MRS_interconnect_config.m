classdef MRS_interconnect_config < handle
  % MRS_interconnect_config : global configuration for MRS_interconnect modules
  %
  % Alexander R. Craven, University of Bergen, 2018-10-17


  methods(Static)

    function ret=get(key,def)
      global MRS_interconnect_config_cache;
      
      if isempty(MRS_interconnect_config_cache) % build and cache config {{{

        cfg=struct();

        % we have the possibility for some context-specific configurations...
        cfg.within_gannet=0;

        callstack=dbstack(); % we'll use the callstack to determine our running context

        for ii=1:length(callstack)
          % If we're running within Gannet, adjust some default behaviours accordlingly...
          if strncmp('Gannet',callstack(ii).name,6)
            cfg.within_gannet=1;
          end
        end

        cfg.output_folder_relative=1; % 0 : output within folder specified in output_folder_base; 1 : output relative to input file name

        cfg.output_folder_base=[];    % empty : output to subfolder of current working dir (at runtime, not startup); otherwise : output goes to the specified folder

        if cfg.within_gannet
          % Gannet's standard behaviour is to save results relative to the working directory, not the source.
          cfg.output_folder_relative=0;
          cfg.output_folder_base=[];
        end

        cfg.do_automatic_corrections=1; % 1: where possible, validate sign and ordering of subspectra and correct if necessary.

        cfg.command_tarquin='tarquin'; % Command to execute tarquin; may specify full path if it's not in your PATH variable.

        cfg.global_downscale_ge_gannet=1e11; % For consistency with Gannet; refer bottom of GERead.m

        % LCModel {{{

        if ispc
          %cfg.user_home=[getenv('HOMEDRIVE'),getenv('HOMEPATH')]; 
          cfg.user_home=getenv('USERPROFILE');
        else
          cfg.user_home=getenv('HOME');
        end

        if ispc
          % if this is a windows system, it's probably not going to be running LCModel locally. So instead, set a generic path which might expand to something rational when run on a remote system
          cfg.path_lcmodel='~/.lcmodel';
        else
          cfg.path_lcmodel=fullfile(cfg.user_home,'.lcmodel');
        end


        cfg.command_lcmodel='lcmodel'; % Command to execute lcmodel; may specify full path if it's not in your PATH variable
        % another likely option ": cfg.command_lcmodel=fullfile(cfg.path_lcmodel,'bin','lcmodel')

        cfg.path_lcmodel_basis=fullfile(cfg.path_lcmodel,'basis-sets');

        % Basis sets selected according to TE, edit/diff flags and perhaps vendor; 'target' is not currently used {{{
        % Note that if a suitable basis set is not listed here, then MRS_interconnect_LCModel will search through the configured basis-sets folder for something appropriate

        cfg.basis=datatable_ARC();

        % default configuration : MEGA-PRESS basis sets from Dr Ulrike Dydak / Purdue : http://purcell.healthsciences.purdue.edu/mrslab/basis_sets.html

        % Basis sets for MegaPress DIFF spectra  : edit=1, diff=1
        cfg.basis.append(struct('TE',68, 'target','GABAGlx', 'edit',1, 'diff',1,'vendor','GE','filename',fullfile(cfg.path_lcmodel_basis,'3t_GE_MEGAPRESS_Kaiser_oct2011_1975_diff.basis')));
        cfg.basis.append(struct('TE',68, 'target','GABAGlx', 'edit',1, 'diff',1,'vendor','Siemens','filename',fullfile(cfg.path_lcmodel_basis,'3t_IU_MP_te68_diff_yesNAAG_noLac_Kaiser.basis')));
        cfg.basis.append(struct('TE',68, 'target','GABAGlx', 'edit',1, 'diff',1,'vendor','Philips','filename',fullfile(cfg.path_lcmodel_basis,'3t_philips_MEGAPRESS_Kaiser_oct2011_1975_diff.basis')));

        % Basis sets for MegaPress edit ON half  : edit=1, diff=0
        cfg.basis.append(struct('TE',68, 'target','GABAGlx', 'edit',1, 'diff',0,'vendor','GE','filename',fullfile(cfg.path_lcmodel_basis,'3t_GE_MEGAPRESS_Kaiser_oct2011_75_ppm_inv.basis')));
        cfg.basis.append(struct('TE',68, 'target','GABAGlx', 'edit',1, 'diff',0,'vendor','Siemens','filename',fullfile(cfg.path_lcmodel_basis,'3t_IU_MP_te68_748_ppm_inv.basis')));
        cfg.basis.append(struct('TE',68, 'target','GABAGlx', 'edit',1, 'diff',0,'vendor','Philips','filename',fullfile(cfg.path_lcmodel_basis,'3t_philips_MEGAPRESS_Kaiser_oct2011_75_ppm_inv.basis')));

        % GSH edited...
        cfg.basis.append(struct('TE',131,'target','GSH',     'edit',1, 'diff',1,'filename',fullfile(cfg.path_lcmodel_basis,'MEGAPRESS_GSH_edited_29102014.basis')));

        % Basis sets for regular press/unedited  : edit=0, diff=0
        cfg.basis.append(struct('TE',35, 'target','PRESS',   'edit',0, 'diff',0,'filename',fullfile(cfg.path_lcmodel_basis,'press_te35_3t_01a.basis')));

        % }}}


        if ~exist(cfg.path_lcmodel,'dir')
          warning('LCModel path does not exist');
        end

        if ~exist(cfg.path_lcmodel_basis,'dir')
          warning('LCModel basis set folder does not exist');
        end

        if ~ispc
          % check lcmodel config and adjust if necessary
          [res,txt]=system('which lcmodel');

          if res>0 && exist(fullfile(cfg.path_lcmodel,'bin','lcmodel'),'file')
            warning('LCModel command is not in your path; correcting configuration accordingly');
            cfg.command_lcmodel=fullfile(cfg.path_lcmodel,'bin','lcmodel');
          end;
        end
        MRS_interconnect_config_cache=cfg;
      % }}}
      else % {{{
        cfg=MRS_interconnect_config_cache;
      end % }}}


      if nargin>0
        if nargin>2 && ~isfield(cfg,key) % fallback to default value
          res=def;
        else
          ret=cfg.(key); % no default? raise an error if the key doesn't exist.
        end
      else
        ret=cfg;
      end;
    end %function

    % attach/detach {{{
    %
    % We wish to be able to cache our configuration structure, to avoid having
    % to re-generate each time. However, defining this as a persistent member
    % makes it... a little too persistent (problematic to update/refresh
    % dependent classes). The present solution is to cache the configuration in
    % a global variable, use a reference counter to attach/detach from the
    % configuration, and purge the global storage when no references remain.

    function attach()
      % ATTACH increment reference counter
      global MRS_interconnect_config_cache_refcounter;
      if isempty(MRS_interconnect_config_cache_refcounter)
        MRS_interconnect_config_cache_refcounter=1;
      else
        MRS_interconnect_config_cache_refcounter=MRS_interconnect_config_cache_refcounter+1;
      end
    end

    function detach()
      % DETACH decrement reference counter, and flush cache when it hits 0
      global MRS_interconnect_config_cache_refcounter;
      global MRS_interconnect_config_cache;
      MRS_interconnect_config_cache_refcounter=MRS_interconnect_config_cache_refcounter-1;
      if MRS_interconnect_config_cache_refcounter==0
        MRS_interconnect_config_cache=[];
      end
      if MRS_interconnect_config_cache_refcounter<0
        warning('MRS_interconnect_config ref counter subzero. Something is awry.');
      end
    end
    % }}}
  end %static methods
end
