% Example Seven : Compare Quantification Algorithms
%
% Preprocessed data can be passed through multiple different quantification algorithms, as
% illustrated in this example.
%
% NOTE, for convenience these example scripts re-use variables across successive calls. 
% To start again with a new file, do: 
%
%   <a href="matlab: clear('source_file','Gannet')">clear('source_file','Gannet');</a>  (or simply, 'clear all')
%
% Alexander R. Craven, University of Bergen, 2018-10-17

example_check_paths();

if ~exist('Gannet','var')
  example_file_picker();

  Gannet=MRS_interconnect_Gannet(source_file);
end

% Quantify with Gannet...
results_GannetFit=Gannet.wrap();

% Quantify with LCModel...
LCM_from_Gannet=MRS_interconnect_LCModel(Gannet,'label','ex07_LCModel_from_Gannet');
results_LCModel=LCM_from_Gannet.wrap();

% Qyantify with Tarquin...
TQ_from_Gannet=MRS_interconnect_tarquin(Gannet,'label','ex07_Tarquin_from_Gannet');
results_Tarquin=TQ_from_Gannet.wrap();

% Show results from the different quantifications
disp('Results from LCModel Fit:');
results_LCModel.show();

disp('Results from Tarquin Fit:');
results_Tarquin.show();

% Combine the result tables, and export to a CSV file...
% (unfortunately Gannet fit results have not yet been transformed into table format, hence their omission below)
results_merged=[results_LCModel; results_Tarquin]; 
disp(['Saving output to ' fullfile(pwd,'ex07_results.csv')]);
results_merged.save('ex07_results.csv');

% Also, write out logs for each of the three processing methods
Gannet.report(Gannet.get_filename('.log'));
LCM_from_Gannet.report(LCM_from_Gannet.get_filename('.log'));
TQ_from_Gannet.report(TQ_from_Gannet.get_filename('.log'));
