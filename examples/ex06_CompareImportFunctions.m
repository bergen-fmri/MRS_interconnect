% Example Six   : Compare Import/Preprocess pipelines
%
% This example illustrates how to do side-by-side visual comparison of different import/preprocess
% pipelines.
%
% NOTE, for convenience these example scripts re-use variables across successive calls. 
% To start again with a new file, do: 
%
%   <a href="matlab: clear('source_file','Gannet','FIDA')">clear('source_file','Gannet','FIDA');</a>  (or simply, 'clear all')
%
% Alexander R. Craven, University of Bergen, 2018-10-17

example_check_paths();

if ~exist('Gannet','var') || ~exist('FIDA','var')
  example_file_picker();

  % Load the same file with FID-A and Gannet pipelines...
  FIDA=MRS_interconnect_FIDA(source_file);
  Gannet=MRS_interconnect_Gannet(source_file);
end

% Compare (visually) the preprocessor output
Gannet.compare(FIDA);

disp('Press a key to continue with next comparison...')
pause();
statusbar(''); % (pause function doesn't manage to clear status correctly...)

% Now, preprocess the GannetLoad data with FIDA... (this doesn't work proberly, yet!)
Gannet_FIDA_Hybrid=MRS_interconnect_FIDA(Gannet,'label','FIDA_preproc_after_GannetLoad');
Gannet_FIDA_Hybrid.preprocess();

% ...and compare with standard Gannet preprocessor
Gannet.compare(Gannet_FIDA_Hybrid);

% ...furthermore, compare with vendor preprocessor (if available...)
if exist('MRS_interconnect_GE')
  disp('Press a key to continue with next comparison...')
  pause();
  statusbar('');
  if MRS_interconnect_GE.check_support('import')
    GE=MRS_interconnect_GE('./sample_data/P38400.7');
    Gannet.compare(GE);
  else
    disp('GE preprocessor not available.');
  end
end
