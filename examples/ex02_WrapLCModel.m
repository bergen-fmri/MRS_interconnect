% Example Two   : Wrap LCModel
%
% Modules which support quantification offer three core functions:
%
%   .write() will prepare the data in an appropriate format for quantification by the nominated program
%   .run()   will execute the appropriate program (where possible)
%   .read()  imports the output from quantification
%
% The .wrap() function calls these three functions in sequence, although you can of course use
% .write() and .read() separately if necessary (if you wish to do further processing, or invoke
% quantification in a particular way).
%
% See also: MRS_interconnect_base.wrap, MRS_interconnect_LCModel.write,
% MRS_interconnect_LCModel.run, MRS_interconnect_LCModel.read, MRS_interconnect_config
%
% NOTE, for convenience these example scripts re-use variables across successive calls. 
% To start again with a new file, do: 
%
%   <a href="matlab: clear('source_file','MRS_struct')">clear('source_file','MRS_struct');</a>  (or simply, 'clear all')
%
% Alexander R. Craven, University of Bergen, 2018-10-17

example_check_paths();

if ~exist('MRS_struct','var')
  example_file_picker();

  paired_water_reference=MRS_interconnect_base.find_paired_reference(source_file);

  % Load data with Gannet
  if isempty(paired_water_reference)
    MRS_struct=GannetLoad({source_file});
  else
    MRS_struct=GannetLoad({source_file},{paired_water_reference});
  end

  % you could also use the MRS_interconnect_Gannet module to load the data; in this case, the paired water reference is automatically detected.
  % eg, Gannet=MRS_interconnect_Gannet(source_file);
  %        LCM=MRS_interconnect_LCModel(Gannet);
end

% Load the existing MRS_struct into the LCModel module; the "label" parameter determines the output folder name.
LCM=MRS_interconnect_LCModel(MRS_struct,'label','ex02_LCModel_from_Gannet');

% Export to LCModel, quantify with default settings, and import the results:
LCM_results=LCM.wrap();

% Alternatively, you can override specific control parameters. For example to quantify without soft constraints:
LCM_results=LCM.wrap('control',{'NRATIO',0});

% Parameters should contain quotes, if these would be necessary within the control file. For example, to specify a specific basis set:
% LCM_results=LCM.wrap('control',{'NRATIO',0,'FILBAS','"/path/to/basis_set.basis"'});
%... on the subject of basis sets, the better place to set these is in the standard configuration file, MRS_interconnect_config.m



disp('----- Summary of Processing -----');
LCM.report();

% Display the results table
LCM_results.show();

% Also, save the report to a file (..../ex02_LCModel_from_Gannet/(source).log)
LCM.report(LCM.get_filename('.log'));

% Save results
output_filename=LCM.get_filename('results.csv'); % get_filename determines appropriate folder prefix
disp(['Saving output to ' output_filename]);
LCM_results.save(output_filename);
