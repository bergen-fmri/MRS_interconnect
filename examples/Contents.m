% EXAMPLES illustrates some basic usage patterns
%
% Files
%   ex01_ExportGannetToLCModel           - Example One   : Export from Gannet MRS_struct to LCModel
%   ex02_WrapLCModel                     - Example Two   : Wrap LCModel
%   ex04_FID_A                           - Example Four  : Use FID-A to import and pre-process data, before quantifying with LCModel.
%   ex05_Hybrid                          - Example Five  : Use Gannet for reading data, and FID-A for processing
%   ex06_CompareImportFunctions          - Example Six   : Compare Import/Preprocess pipelines
%   ex07_CompareQuantificationAlgorithms - Example Seven : Compare Quantification Algorithms
%   ex08_BatchProcessing                 - Example Eight : Batch processing of all spectra within a nominated folder
%   ex09_BatchProcessing_Expanded        - Example Nine : Batch processing of all spectra within a nominated folder, with various methods
%   example_check_paths                  - Check that paths to dependencies are set correctly for the example code
%   example_file_picker                  - Shows filepicker for example code
