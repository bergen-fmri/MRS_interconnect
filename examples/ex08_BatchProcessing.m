% Example Eight : Batch processing of all spectra within a nominated folder
%
% Alexander R. Craven, University of Bergen, 2018-10-17

example_check_paths();

dn=uigetdir('.','Choose a directory to start searching');

files={};

lim=1000;

patterns={'*.7','*.sdat', '*.dat'}; % specify filename patterns here (case-insensitive match)

if (ischar(dn) && ~isempty(dn))

%----- Part One : Recursive search for spectroscopy data under the selected folder -------------{{{
  disp([ 'Searching for spectroscopy data under ' dn])
  dirqueue={dn};
  ptr=1;
  while(length(dirqueue)>=ptr)
    thisdir=dirqueue{ptr};
    disp(['Looking in: ' thisdir]);
    ptr=ptr+1;
    items=dir(thisdir);
    for item=items'
      if item.isdir && (item.name(1)~='.')
        if length(dirqueue)<lim
          dirqueue{end+1}=fullfile(thisdir,item.name);
        else
          warning(['Search limit reached; skipping folder ' fullfile(thisdir,item.name)]);
        end
      elseif ~item.isdir
        [ip,in,ie]=fileparts(item.name);
        for pi=1:length(patterns)
          p=patterns{pi};
          [pp,pn,pe]=fileparts(p);
          if (strcmp(pn,'*')||strcmpi(in,pn)) && (strcmp(pe,'*')||strcmpi(ie,pe))
            % check whether this file is a water reference...
            found=fullfile(thisdir,item.name);
            found_ref=MRS_interconnect_base.find_paired_reference(found);
            if ~isempty(found_ref) && strcmp(found,found_ref)
              disp([' - Skip:  ' item.name ' (appears to be reference scan)'])
            else
              disp([' - Found: ' item.name])
              files{end+1}=found;
            end
          end
        end
      end
    end
  end

  clear('pp','pn','pe','p','ptr','dirqueue','patterns','item','items','thisdir','ip','in','ie','lim','pi','found','found_ref');

  for i=1:length(files)
    disp(sprintf('%03d : %s', i, files{i}));
  end

% }}}---------------------------------------------------------------------------------------------

  failures=containers.Map();

  if length(files)>0
    cont=input(sprintf('Ready to process %d spectra; do you wish to continue? (y/Y for yes, anything else for no) ',length(files)),'s');
    if strcmpi(cont,'y')

      all_results=datatable_ARC();

%----- Part Two : Recurse over file list, quantify, and add to the global results table -------{{{
      for i=1:length(files)
        % cleanup figures from previous run(s)
        close all; 

        try
          % Load the data with Gannet
          Gannet=MRS_interconnect_Gannet(files{i});
          % Export to LCModel, quantify, and import the results:
          LCM=MRS_interconnect_LCModel(Gannet,'label','ex08_LCModel_Batch');
          % Export to LCModel, quantify, and import the results:
          LCM_results=LCM.wrap();

          % Write report (log) file
          LCM.report(LCM.get_filename('.log'));

          % Display results table for this subject
          LCM_results.show();

        catch Ex
          % track any failures -- but don't allow a single failed subject to derail the entire batch
          ex_formatted=formatting.format_exception(Ex);
          failures(files{i})=ex_formatted;
          warning(ex_formatted);
          warning(['Processing failed for ' files{i}]);
        end

        % append to summary datatable
        all_results.append(LCM_results);
      end

% }}}---------------------------------------------------------------------------------------------

%----- Part Three : Show results, save results, report problems -------------------------------{{{
      disp('Results for all scans:');
      all_results.show();

      disp(['Saving results to: ' fullfile(dn,'ex08_LCModel_Batch.csv')]);
      all_results.save(fullfile(dn,'ex08_LCModel_Batch.csv'));

      failed_items=failures.keys();
      if length(failed_items)>0
        disp('Processing failed for the following items: ');
        for j=1:length(failed_items)
          disp(sprintf(' - %s : %s', failed_items{j},failures(failed_items{j})));
        end
      end
% }}}---------------------------------------------------------------------------------------------

    end
    clear('cont','i');
  end
end
