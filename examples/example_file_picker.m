% EXAMPLE_FILE_PICKER Shows filepicker for example code
%
% Note this is only run when a valid source_file variable is not set: ie, you will only be prompted
% once, unless you <a href="matlab: clear('source_file','MRS_struct')">clear source_file</a>

if ~exist('source_file','var') || ~exist(source_file,'file')
  source_file=[];
end


if isempty(source_file)
  %source_file='./sample_data/P38400.7';
  source_file='/scratch/mugaba/subs/G4_MP/S02/S02_GABA_68.7';
end

if ~exist(source_file,'file')
  [basename, dirname, ix] = uigetfile({'*.7;*.SDAT;*.sdat;*.DAT;*.dat','Spectroscopy data';'*.*','All files'},'Pick a file'); % FilterSpec is annoyingly case-sensitive
  if isstr(basename) && ~isempty(basename) % basename==0 in case of cancellation
    source_file=fullfile(dirname,basename);
  else
    error('no file picked. aborting');
  end
end

