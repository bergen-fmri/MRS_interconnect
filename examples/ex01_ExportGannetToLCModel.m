% Example One   : Export from Gannet MRS_struct to LCModel
%
% All MRS_interconnect modules can accept an existing Gannet-compatible MRS_struct as input.
%
% This module illustrates use of the MRS_interconnect_LCModel module to write LCModel-format RAW
% data (and suggested control files), for data which has previously been read with GannetLoad.
%
% See also: MRS_INTERCONNECT_LCMODEL, MRS_INTERCONNECT_LCMODEL.WRITE, GANNETLOAD
%
% NOTE, for convenience these example scripts re-use variables across successive calls. 
% To start again with a new file, do: 
%
%   <a href="matlab: clear('source_file','MRS_struct')">clear('source_file','MRS_struct');</a>  (or simply, 'clear all')
%
% Alexander R. Craven, University of Bergen, 2018-10-17

example_check_paths();

if ~exist('MRS_struct','var')

  example_file_picker();

  paired_water_reference=MRS_interconnect_base.find_paired_reference(source_file);

  % Load data with Gannet
  if isempty(paired_water_reference)
    MRS_struct=GannetLoad({source_file});
  else
    MRS_struct=GannetLoad({source_file},{paired_water_reference});
  end

end

LCM=MRS_interconnect_LCModel(MRS_struct,'label','ex01_LCmodel_from_Gannet');

% Write LCModel-compatible RAW and REF spectra, for all components of the incoming MRS_struct
% Multi-voxel and multi-target (HERMES) data will be split into relevant components
LCM.write();

disp('----- Summary of Processing -----');
LCM.report();

% Also, save the report to a file (..../ex01_LCmodel_from_Gannet/(source).log)
LCM.report(LCM.get_filename('.log'));
