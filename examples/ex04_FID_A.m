% Example Four  : Use FID-A to import and pre-process data, before quantifying with LCModel.
%
% This is not particularly recommended; see example five for a more robust approach.
%
% Unfortunately, the current (2018-09) FID-A release has somewhat limited file format support, and
% some known bugs. Check the warnings (including those reported by example_check_paths) for more
% information. GannetLoad currently offers broader support for different MP implementations.
%
% NOTE, for convenience these example scripts re-use variables across successive calls. 
% To start again with a new file, do: 
%
%   <a href="matlab: clear('source_file','Gannet','FIDA')">clear('source_file','Gannet','FIDA');</a>  (or simply, 'clear all')
%
% Alexander R. Craven, University of Bergen, 2018-10-17

example_check_paths();

if ~exist('FIDA','var')
  example_file_picker()

  % Load data with FIDA
  FIDA=MRS_interconnect_FIDA(source_file);
end

% Quantify data with LCModel. The optional 'label' parameter determines the output folder.
LCM=MRS_interconnect_LCModel(FIDA,'label','ex04_LCM_fit_from_FIDA_import');

LCM_results=LCM.wrap();

disp('----- Summary of Processing -----');
LCM.report();

% Also, save the report to a file (..../ex04_LCM_fit_from_FIDA_import/(source).log)
LCM.report(LCM.get_filename('.log'));

% Display the results table
LCM_results.show();
