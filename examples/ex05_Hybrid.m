% Example Five  : Use Gannet for reading data, and FID-A for processing
%
% Current versions of Gannet provide better support for the diversity of megapress data formats than
% current versions of FID-A.
%
% This example shows how to combine the two, using GannetLoad to import data and FID-A for
% preprocessing, before quantification in LCModel.
%
% NOTE, for convenience these example scripts re-use variables across successive calls. 
% To start again with a new file, do: 
%
%   <a href="matlab: clear('source_file','Gannet')">clear('source_file','Gannet');</a>  (or simply, 'clear all')
%
% Alexander R. Craven, University of Bergen, 2018-10-18

example_check_paths();

if ~exist('Gannet','var')
  example_file_picker()

  % Load data with Gannet (MRS_interconnect_Gannet will wrap GannetLoad)
  Gannet=MRS_interconnect_Gannet(source_file);
end

% ...then FID_A for preprocessing

Gannet_FIDA_Hybrid=MRS_interconnect_FIDA(Gannet,'label','FIDA_preproc_after_GannetLoad');
Gannet_FIDA_Hybrid.preprocess();

Gannet.compare(Gannet_FIDA_Hybrid);

% ...and finally, LCModel output and quantification

LCM=MRS_interconnect_LCModel(Gannet_FIDA_Hybrid,'label','ex05_LCModel_from_FIDA_preproc_after_GannetLoad');
LCM.wrap();

disp('----- Summary of Processing -----');
LCM.report();
