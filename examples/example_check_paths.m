function example_check_paths()
  % EXAMPLE_CHECK_PATHS Check that paths to dependencies are set correctly for the example code
  %
  % This function checks for availability of additional packages which may be required for correct functionality.
  %
  % Furthermore, it checks for a couple of known bugs and compatibility issues with certain versions of those packages.

  if exist('MRS_interconnect_LCModel')==0
    p=mfilename('fullpath');
    [base,x_,xx_]=fileparts(p);
    [parent,x_,xx_]=fileparts(base);
    if exist(fullfile(parent,'MRS_interconnect_LCModel.m'))
      warning(['MRS_interconnect modules were not set in the matlab path; adding these automatically now: ' parent ]);
      addpath(parent)
    else
      error('MRS_interconnect modules could not be found. Please add these to the Matlab search path.');
    end 
  end
  if exist('io_loadspec_GE')==0
    warning('io_loadspec_GE function not available. Please ensure that FID-A is installed in your Matlab search path.');
  else
    [fida_io,x_,xx_]=fileparts(which('io_loadspec_GE'));
    [fida_base,x_,xx_]=fileparts(fida_io);

    if (exist('GELoad')==0)
      warning('FID-A inputOutput/gannetTools subfolder should also be included in the search path.');
      addpath(fullfile(fida_base,'inputOutput','gannetTools'));
    end

    if (exist('run_megapressproc_GEauto')==0)
      warning('FID-A exampleRunScripts subfolder should also be included in the search path.');
      addpath(fullfile(fida_base,'exampleRunScripts'))
    end
  end

  if exist('GannetLoad')==0
    warning('GannetLoad function does not appear to be available. Please ensure that Gannet is in your Matlab search path.');
  end

  if exist('statset')==0
    warning('Gannet SpectralRegistration may depend on matlab''s statistics and machine learning toolbox, which does not appear to be installed.');
  end

  if exist('padarray')==0
    warning('FID-A may depend on the Image Processing Toolbox, which does not appear to be installed.');
  end


  % now, try to catch some known bugs and/or compatibility issues with dependencies...

  if exist('SiemensTwixRead') && ~exist('reorderstructure')
    str_fn=which('SiemensTwixRead');
    fid=fopen(str_fn,'r');
    linenum=0;
    rslim=2;
    while 1
      line=fgetl(fid);
      if isnumeric(line) && line<0
        break
      end
      linenum=linenum+1;
      if strfind(line,'reorderstructure')
        warning(sprintf('Current implementation of SiemensTwixRead appears to reference reorderstructure function, which is not defined. Please check %s %s',str_fn,formatting.editor_link(str_fn,linenum,sprintf('line %d',linenum))));
        rslim=rslim-1;
        if rslim==0
          break;
        end
      end
    end
    fclose(fid);
  end

  if exist('GELoad') && exist('io_loadspec_GE')
    geload_fn=which('GELoad'); %fullfile(fida_base,'inputOutput','gannetTools','GELoad.m');
    fid=fopen(geload_fn,'r');
    ii_assigned=0;
    ii_referenced=0;
    linenum=0;
    while 1
      line=fgetl(fid);
      if isnumeric(line) && line<0
        break
      end
      linenum=linenum+1;
      if ~ii_assigned
        if regexp(line,'^[\W]*ii[ ]*=')
          ii_assigned=linenum;
        end;
      else
        if strfind(line,'(ii)')
          ii_referenced=linenum;
          break;
        end
      end
    end
    fclose(fid);

    if (ii_referenced>0 && ii_assigned==0)
      warning(sprintf('Some FID-A GELoad implementations fail due to an unassigned "ii" variable, for the nechoes>1 case. Please check %s: remove (ii) indexing at %s, or set ii=1 near the top of that function',geload_fn,formatting.editor_link(geload_fn,ii_referenced,sprintf('line %d', ii_referenced))));
    end
    %has_ii_indexing=find(cellfun(@(x) length(strfind(x,'(ii)')),lines{:}))
  end

  if exist('datetime')==0 && exist('io_loadspec_Twix')
    mapvbvd_fn=which('mapVBVD');
    fid=fopen(mapvbvd_fn,'r');
    linenum=0;
    while 1
      line=fgetl(fid);
      if isnumeric(line) && line<0
        break
      end
      linenum=linenum+1;
      if strfind(line,'datetime')
        warning(sprintf('Current FID-A implementation of mapVBVD (used by siemens twix import) depends on the datetime function, which is missing from your system (requires Matlab R2014b or more recent). If Twix import fails for this reason, ensure that Gannet''s version of mapVBVD is being used (or disable the offending block, which may be around %s).',formatting.editor_link(mapvbvd_fn, linenum, sprintf('line %d',linenum))));
        break;
      end
    end
    fclose(fid);
  end
end
