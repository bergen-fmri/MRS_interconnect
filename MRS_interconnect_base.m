classdef MRS_interconnect_base < handle
  % MRS_interconnect_base : base class for application-specific MRS_interconnect modules
  %
  % This class provides core functionality and "skeleton" functions, as a basis for other modules.
  %
  % MRS_interconnect_base Methods:
  %
  %   MRS_interconnect_base - constructor
  %
  %   ...and some others, see <a href="matlab: doc MRS_interconnect_base">doc MRS_interconnect_base</a> for more.
  %
  % See Also: MRS_interconnect
  %
  %---------------------------------------------------------
  %  2018-09-20  Alexander R. Craven (University of Bergen)
  %---------------------------------------------------------


  properties (Access=protected)
    MRS_struct % Gannet-style MRS_struct is used internally to hold basic parameters
    vendor     % don't use this yet.
    local      % don't use this yet either.
    loginfo    % detailed history of processing steps
    provenance % brief history of critical processing stages
    label=[];   % a label for tagging data coming from this instance (eg, output folder names and row labels)
  end

  methods % {{{

    function self=MRS_interconnect_base(d,varargin)
      % MRS_interconnect_base constructor, stores initial parameters.
      %
      % MRS_interconnect_base(FILENAME) attempts to load FILENAME using this module's standard
      % import function
      %
      % MRS_interconnect_base(MRS_struct) initialises this object using parameters from
      % Gannet-compatible MRS_struct
      %
      % MRS_interconnect_base(MRS_interconnect_base) initialise this object using parameters
      % imported from another derived object. Use this form to mix different import, processing and
      % quantification modules.
      %
      % See Also: import, can_import

      MRS_interconnect_config.attach(); % let our configuration cache know that we'll be needing it...
      self.provenance={};
      self.loginfo=[];
      self.vendor=struct();

      self.label=self.get_short_module_name(); % default subfolder is name of the module

      % process variable arguments...
      if nargin>1
        while ~isempty(varargin) % {{{
          switch lower(varargin{1})
            case 'label'
              self.label=varargin{2};
            otherwise
              error(['Unknown option: ' varargin{1}]);
            end % switch
          varargin(1:2)=[];
        end % while }}}
      end

      if nargin>0
        if isa(d,'MRS_interconnect_base') % clone from another MRS_interconnect_base object
          self.loginfo=d.loginfo;
          self.provenance=d.provenance;
          self.log(sprintf('Copy from %s',class(d)));
          d.log(sprintf('(copied to %s)',class(self)));
          self.MRS_struct=d.MRS_struct;
        elseif self.can_import(d) % import data from a compatible format
          if ischar(d)
            self.log(sprintf('Import from %s',d))
            [x_,basename,xx_]=fileparts(d);
            self.log_provenance(['Load ' basename]);
          else
            self.log(sprintf('Import from [%s]',class(d)));
          end
          self.import(d);
        elseif isa(d,'struct') % or trivially store an MRS_struct
          self.log('Create from MRS_struct');
          self.MRS_struct=d;
        else
          error('MRS_interconnect_base constructor could not handle this input.');
        end
      end
    end

    function delete(self)
      % DELETE clean up

      MRS_interconnect_config.detach(); % let our configuration cache know that we no longer require its services.
    end

    function report(self,target)
      % REPORT displays the log for this object, line-by-line
      %
      % REPORT() shows each line of the log, formatted, on the standard output
      % REPORT(TARGET) writes the formatted log to file specified by TARGET
      %
      % See Also: report_line, log
      
      if nargin<2
        f=1;
        plaintext=0;
      else
        f=fopen(target,'w');
        plaintext=1;
      end
      % output to screen
      for i=1:length(self.loginfo)
        ln=self.report_line(self.loginfo(i));
        if plaintext
          % remove tags which may have been used for console display
          ln=regexprep(ln,'<(/?)a.*?>','');
        end
        fprintf(f,'%s\n',ln);
      end

      if f>2
        fclose(f);
      end
    end

    function str=report_line(self,entry)
      % REPORT_LINE returns a single log entry formatted as a line of text
      %
      % See Also: report, log
      cl=entry.time;
      clstr=sprintf('%04u-%02u-%02u %02u:%02u:%02u',cl(1),cl(2),cl(3),cl(4),cl(5),floor(cl(6)));
      module=strrep(entry.module,'MRS_interconnect_','');
      str=sprintf('%-20s %10s : %s',clstr,module,entry.detail);
    end

    function str=get_short_module_name(self,str)
      % GET_SHORT_MODULE_NAME returns a short string identifying this module
      %
      % GET_SHORT_MODULE_NAME() returns the class name without the standard prefix
      % GET_SHORT_MODULE_NAME(STR) returns STR with the standard prefix removed.

      if nargin<2
        str=class(self);
      end
      str=strrep(str,'MRS_interconnect_',''); % remove standard prefix from name
      str=strrep(str,'MRS_interop_','');      % remove older prefix from name
    end

    function log_provenance(self,tag)
      % LOG_PROVENANCE maintains a succinct listing of key processing steps
      % LOG_PROVENANCE(TAG) adds TAG to the log; the TAG parameter should identify the step in short form (perhaps an abbreviated keyword), for example "load", "quant", "preproc"

      self.provenance{end+1}=[self.get_short_module_name() ' ' tag];
    end

    function str=get_provenance_string(self)
      % GET_PROVENANCE_STRING returns the succinct provenance log information formatted as a single line of test
      str='';
      for i=1:length(self.provenance)
        str=[str self.provenance{i} ' => '];
        %str=[str strrep(self.provenance{i},' ','_') '_' ]
      end
      str=str(1:end-4);
    end

    function log(self,desc,type)
      % LOG saves timestamped details of processing steps and outcomes
      % LOG(DESC,TYPE) : DESC parameter may contain arbitrary text (single-line, with no particular constraints on length).
      % Timestamp and the originating module name are also recorded.

      if nargin<3
        type=[];
      end
      entry=struct(...
        'time',clock(),...
        'module',class(self),...
        'detail',desc,...
        'type',type);
      if isempty(self.loginfo)
        self.loginfo=entry;
      else
        self.loginfo(end+1)=entry;
      end
      disp(self.report_line(entry));
      if ~isempty(type)&&(strncmpi('warn',type,4)||strncmpi('err',type,3))
        warning(desc);
      end
    end

    function obj=check_vendor_extensions(self)
      % CHECK_VENDOR_EXTENSIONS Don't use this yet.

      if isempty(self.vendor)||(isstruct(self.vendor)&&length(fieldnames(self.vendor))==0) % only check if we haven't already loaded some vendor extensions
        if isstruct(self.MRS_struct) && strcmp(self.MRS_struct.p.vendor,'GE') && ~isa(self,'MRS_interconnect_GE')
          warning('creating additional vendor-specific interconnect');
          self.vendor.GE=MRS_interconnect_GE(self.get_filename());
        end
      end
    end

    function res=write(self)
      % WRITE write spectra (and possibly associated metadata) in application-specific format, in preparation for analysis (run)
      %
      % See also: RUN, READ
      error('Not implemented.');
    end

    function res=run(self)
      % RUN call (external) program to perform analysis data which has been previously written by a call to write()
      %
      % See also: WRITE, READ
      error('Not implemented.');
    end

    function res=read(self)
      % READ import results from external analysis (performed by a call to run)
      %
      % See also: WRITE, RUN
      error('Not implemented.');
    end

    function res=import(self)
      % import data in the format specific to this application
      error('Not implemented.');
    end;

    function res=wrap(self,varargin)
      % WRAP performs standard sequence of processing steps
      %
      % This method invokes the write, run and read methods in series, to export data, run the analysis and return the associated results.
      %
      % See also: WRITE, RUN, READ

      self.log('Begin wrap: write -> run -> read')
      self.write(varargin{:});
      self.run(varargin{:});
      res=self.read(varargin{:});
      % self.report();
    end

    function fn=get_filename(self,suffix,folder)
      % GET_FILENAME returns a standardised filename (with full path) having the specified suffix
      %
      % GET_FILENAME() returns the source filename (ie, metabfile)
      %
      % GET_FILENAME(SUFFIX) returns a name of the form A/B/CD, where:
      %    A is the full path to the source data for this instance
      %    B is the "label" for this instance
      %    C is the base name for the source data (filename without path or extension), and
      %    D is the specified suffix
      %    The associated folder (A/B) is created if necessary.

      [in_base,in_name,in_ext]=fileparts(self.MRS_struct.metabfile{1});

      if nargin<3
        folder=self.label;
      end

      if nargin<2
        % no suffix... return the input filename
        fn=self.MRS_struct.metabfile{1};
        return;
      end

      if MRS_interconnect_config.get('output_folder_relative',1)
        out_base=in_base;
      else
        out_base=MRS_interconnect_config.get('output_folder_base',[]);
        if isempty(out_base)
          out_base=pwd();
        end
      end

      if ~isempty(folder)
        out_base=fullfile(out_base,folder);
      end

      if ~exist(out_base,'dir')
        disp(['Output folder does not exist; creating ' out_base]);
        mkdir(out_base);
      end

      fn=fullfile(out_base,[in_name suffix]);
    end

    function MRS_struct=get_MRS_struct(self)
      MRS_struct=self.MRS_struct;
    end;

    function filename_variant=get_filename_variant(self,varargin)
      [vox,target,spec]=self.decode_selector_args(varargin{:});
      filename_variant=[ '.' vox '.' target '.' spec ];
    end

    function [vox,target,spec]=decode_selector_args(self,varargin) % {{{
      % determine which subspectrum has been requested... expanding numeric indices where relevant
      % returns [vox, target, spec] in that order.

      vox=1;
      target=1;
      spec='diff';
      while ~isempty(varargin) % {{{
        switch lower(varargin{1})
          case 'target'
            target=varargin{2};
          case 'vox'
            vox=varargin{2};
          case 'spec'
            spec=varargin{2};
          % ignore other args
        end % switch
        varargin(1:2)=[];
      end % while }}}

      if isnumeric(target)
        if target==1
          tk='target';
        else
          tk=sprintf('target%d',target);
        end
        target=self.MRS_struct.p.(tk);
      end

      if isnumeric(vox)
        vox=self.MRS_struct.p.Vox{vox};
      end
    end % }}}

    function [variations,varargout]=get_subspec_variations(self,varargin)
      % get_subspec_variations returns a nested cell array, wherein each row contains a unique combination of voxel (for PRIAM/multivoxel data), target (for HERMES/multiply-edited data) and subspectrum (on/off/diff).
      %
      % Rows contain the associated parameters as a cell array of key, value pairs, eg {'spec','diff,'voxel','vox1','target','GABAGlx'} which may be passed directly as method arguments, eg: LCModel.run(variations{i}{:})
      % 'spec', 'vox', or 'target' parameters supplied to this function will result in only combinations matching that parameter being returned (if all three are specified, then the resultant cell array will contain a single row with the specified values).

      specs={'diff','off','on'};

      voxen=self.MRS_struct.p.Vox;
      if self.MRS_struct.p.PRIAM==0
        voxen(2:end)=[];
      end

      targets={};
      for t=1:(1+(self.MRS_struct.p.HERMES>0))
        if t==1
          targets{end+1}=self.MRS_struct.p.target;
        else
          targets{end+1}=self.MRS_struct.p.target2;
        end
      end

      % use standard decoder function for any supplied args, but only take those which are explicitly provided (ie, skip defaults)
      [vox_,target_,spec_]=self.decode_selector_args(varargin{:});

      remaining_args={};

      while ~isempty(varargin)
        switch lower(varargin{1})
          case 'target'
            targets={target_};
          case 'vox'
            voxen={vox_};
          case 'spec'
            specs={spec_};
          otherwise
            if nargout>1
              remaining_args{end+1}=varargin{1};
              remaining_args{end+1}=varargin{2};
            end
        end
        varargin(1:2)=[];
      end

      variations={};
      for s=1:length(specs)
        spec=specs{s};
        for v=1:length(voxen)
          vox=voxen{v};
          for t=1:length(targets)
            target=targets{t};
            variations{end+1}={'spec',spec,'vox',vox,'target',target};
          end
        end
      end
      if (nargout>1)
        varargout{1}=remaining_args;
      end
    end

    function str=get_subspec_string(self,varargin)
      % GET_SUBSPECT_STRING returns a short string describing the selected subspectrum
      %
      % GET_SUBSPECT_STRING('spec',SPEC,'vox',VOX,'target',TARGET) returns a short string
      % identifying the specified spectrum (on/off/diff), voxel (PRIAM data only, named according to
      % MRS_struct) and target (HERMES data only, named according to MRS_struct)

      [vox,target,spec]=self.decode_selector_args(varargin{:});
      str='';
      if self.MRS_struct.p.PRIAM>0
        % only include voxel prefix if this is infact multivoxel data
        str=[str vox ' '];
      end
      if self.MRS_struct.p.HERMES>0
        % if this is multi-edited data, include target
        str=[str target ' '];
      end
      % include ON/OFF/DIFF in all cases
      str=[str upper(spec)];
    end

    function compare(self, other)
      % COMPARE performs a quick visual comparison of two datasets

      a=self.MRS_struct;
      b=other.MRS_struct;

      av=self.get_subspec_variations();
      bv=other.get_subspec_variations();

      [vox_a,targ_a,spec_a]=self.decode_selector_args(av{1}{:});
      [vox_b,targ_b,spec_b]=self.decode_selector_args(bv{1}{:});

      ppm_a=a.spec.freq;
      ppm_b=b.spec.freq;

      range_ref_a=ppm_a >= 4.4 & ppm_a <= 5.1;
      range_met_a=ppm_a >= 0.0 & ppm_a <= 6.0;
      range_ref_b=ppm_b >= 4.4 & ppm_b <= 5.1;
      range_met_b=ppm_b >= 0.0 & ppm_b <= 6.0;

      range_scale_a=ppm_a>=0.0 & ppm_a <= 4.0;
      range_scale_b=ppm_b>=0.0 & ppm_b <= 4.0;

      title_a=['A: ' self.label ' ' a.p.vendor ' ' self.get_short_module_name(a.version.load)];
      title_b=['B: ' other.label ' ' b.p.vendor ' ' self.get_short_module_name(b.version.load)];

      MRS_plot.figure();

      subplot(2,6,[1,2]);
      plot(a.spec.freq(range_ref_a),real(a.spec.(vox_a).water(range_ref_a)),'b',b.spec.freq(range_ref_b),real(b.spec.(vox_b).water(range_ref_b)),'r--');
      title('water');
      set(gca,'XDir','reverse');
      legend({title_a,title_b},'Interpreter','none');

      maxa=max(abs(a.spec.(vox_a).(targ_a).on(range_scale_a)));
      maxb=max(abs(b.spec.(vox_b).(targ_b).on(range_scale_b)));

      mina=min(real(a.spec.(vox_a).(targ_a).on(range_scale_a)));
      minb=min(real(b.spec.(vox_b).(targ_b).on(range_scale_b)));
      yl=[min(mina,minb),max(maxa,maxb)];
      yl=yl+[-0.1,0.1]*(yl(2)-yl(1)); % give 10% margin in each direction

      subplot(2,6,[3,4]);
      plot(a.spec.freq(range_met_a),real(a.spec.(vox_a).(targ_a).on(range_met_a)),'b',b.spec.freq(range_met_b),real(b.spec.(vox_b).(targ_b).on(range_met_b)),'r--');
      MRS_plot.format_freq();
      MRS_plot.gaba_markers();

      title('on');
      set(gca,'YLim',yl);

      subplot(2,6,[5,6]);
      plot(a.spec.freq(range_met_a),real(a.spec.(vox_a).(targ_a).off(range_met_a)),'b',b.spec.freq(range_met_b),real(b.spec.(vox_b).(targ_b).off(range_met_b)),'r--');
      title('off');
      set(gca,'YLim',yl);
      MRS_plot.format_freq();
      MRS_plot.gaba_markers();

      subplot(2,6,[7:9]); 
      plot(b.spec.freq(range_met_b),real(b.spec.(vox_b).(targ_b).on(range_met_b)),'Color',[1,.8,.6]); hold on;
      plot(a.spec.freq(range_met_a),real(a.spec.(vox_a).(targ_a).on(range_met_a)),'b',...
        a.spec.freq(range_met_a),real(a.spec.(vox_a).(targ_a).off(range_met_a)),'k--');
        %b.spec.freq(range_met_b),b.spec.(vox_b).(targ_b).on(range_met_b),'r:'  ...
      set(gca,'YLim',yl);
      title('self on vs off');
      MRS_plot.format_freq();
      MRS_plot.gaba_markers();

      subplot(2,6,[10:12]); 
      plot(a.spec.freq(range_met_a),real(a.spec.(vox_a).(targ_a).diff(range_met_a)),'b',b.spec.freq(range_met_b),real(b.spec.(vox_b).(targ_b).diff(range_met_b)),'r--');
      title('diff');
      MRS_plot.format_freq();
      MRS_plot.gaba_markers();

      MRS_plot.suptitle('Comparison: Metabolite Signal',self.get_provenance_string());
      MRS_plot.export('comparison_metab');

      disp('a/b');
      ratio_water=max(real(a.spec.(vox_a).water))/max(real(b.spec.(vox_b).water))
      ratio_metab=max(real(a.spec.(vox_a).(targ_a).diff(range_met_a)))/max(real(b.spec.(vox_b).(targ_b).diff(range_met_b)))
      if (abs((ratio_metab-ratio_water)/mean([ratio_metab,ratio_water])))>.05
        warning('Quite different water/metab ratios by the two methods!!! This is bad.');
      elseif abs(ratio_water-1)>0.05 || abs(ratio_metab-1)>0.1
        warning('Scaling factors differ between the two methods (but at least the proportion metab to water remains similar).');
      end

      MRS_plot.figure();
      subplot(2,2,1);
      plot(a.spec.freq,real(a.spec.(vox_a).water),'DisplayName','real'); hold on; plot(a.spec.freq,imag(a.spec.(vox_a).water),'g--','DisplayName','imag');
      set(gca,'XLim',[-1,6])
      MRS_plot.format_freq();
      title(title_a);
      subplot(2,2,2);
      plot(b.spec.freq,real(b.spec.(vox_b).water),'DisplayName','real'); hold on; plot(b.spec.freq,imag(b.spec.(vox_b).water),'g--','DisplayName','imag');
      set(gca,'XLim',[-1,6])
      MRS_plot.format_freq();
      title(title_b);

      subplot(2,2,3);
      plot(real(ifft(ifftshift(a.spec.(vox_a).water))),'DisplayName','real'); hold on; plot(imag(ifft(ifftshift(a.spec.(vox_a).water))),'g--','DisplayName','imag');
      MRS_plot.format_time();
      subplot(2,2,4);
      plot(real(ifft(ifftshift(b.spec.(vox_b).water))),'DisplayName','real'); hold on; plot(imag(ifft(ifftshift(b.spec.(vox_b).water))),'g--','DisplayName','imag');
      MRS_plot.format_time();

      MRS_plot.suptitle('Comparison: Water Reference',self.get_provenance_string());
      MRS_plot.export('comparison_ref');
    end

    function [site,subject,sequence]=decode_session_info(self)
      % DECODE_SESSION_INFO attempts to determine site, subject and session type for the corresponding data file
      %
      % [SITE,SUBJECT,SEQUENCE]=DECODE_SESSION_INFO() scans the filename for this dataset, in search
      % of tokens to identify the site, subject number and session type. This is currently tuned to
      % the Big Gaba naming scheme, eg, Mmm_MP/.../Sss_GABA_68 where Mmm is manufacturer/site
      % identifier starting with one of P, S or G, Sss is subject number and GABA_68 (or similar) is
      % the sequence identifier

      remaining=self.get_filename();
      site=[];
      subject=[];
      sequence=[];
      lim=5;
      while length(remaining)>1 && lim>0
        [remaining,part,xx]=fileparts(remaining);

        if isempty(sequence)
          r=regexp(part,'^S(?<subject>[0-9]+)_(?<sequence>GABA_[0-9]+)$','names','once');
          if ~isempty(r)
            subject=r.subject;
            sequence=r.sequence;
            continue;
          end
        end

        % subjects separated in numbered folders? 
        s=regexp(part,'^S(?<subject>[0-9]+)$','names','once');
        if ~isempty(s)
          if isempty(subject)
            subject=s.subject;
          elseif ~strcmp(subject,s.subject)
            % if we already had a subject number from the filename, check that it's consistent.
            warning('Subject number could not be unambiguously determined from the filename...');
          end
        end

        if isempty(site)
          r=regexp(part,'^(?<site>[PSG][0-9]+)_MP$','names','once');
          if ~isempty(r)
            site=r.site;
            % site should be the last thing we find; stop looking now.
            break;
          end
        end;

        lim=lim-1; % avoid getting stuck if our other tests don't match (the drive specifier can confuse the above on Windows systems)
      end
    end


  end % }}}

  methods(Static)

    function waterfile=find_paired_reference(fn)
      % FIND_PAIRED_REFERENCE : identify the water reference data associated with a particular input (based on common naming patterns)
      %
      % FIND_PAIRED_REFERENCE(FN) looks for a "water reference" dataset corresponding with the
      % metabolite data contained in FN.
      %
      % Common filename veriations are used, either:
      %   - a suffix '_w','_ref','_H2O' or '_vannref' to the filename,
      %   - or (per FID-A example data) a separate parent folder with '_w' suffix
      %
      % If supplied FN is already water-reference data, then same filename is likely to be returned.
      %
      % If no matching file is found, an empty waterfile [] is returned.
      %
      % If supplied FN is in GE .7 format (according to extension), an empty waterfile [] is also
      % returned (this format generally contains metab and reference data in the same file)


      waterfile=[];
      dbg=0;

      [dirname,basename,ext]=fileparts(fn);
      skip_suffixes={'7'}; % formats for which a paired reference is not required.
      if any(strcmp(ext,skip_suffixes)) 
        return
      end

      folder_options={dirname, [dirname '_w']};
      basename_nosuf=regexprep(basename,'_[a-zA-Z0-9]+$','');
      basename_options={basename};

      if ~strcmp(basename,basename_nosuf)
        basename_options{end+1}=basename_nosuf;
      end

      % whilst UiB tends to use a '_vannref' or '_ref' suffix:
      suffix_options={'_w','_vannref','_ref','_H2O'};

      for fi=1:length(folder_options)
        if exist(folder_options{fi},'dir')
          for bi=1:length(basename_options)
            for si=1:length(suffix_options)
              possible_filename=fullfile(folder_options{fi},[ basename_options{bi} suffix_options{si} ext ]);
              if dbg
                disp(['Checking: ' possible_filename]);
              end
              if exist(possible_filename,'file')
                if dbg
                  disp(['Found:    ' possible_filename]);
                end
                waterfile=possible_filename;
                break;
              end % if
            end % for si
            if ~isempty(waterfile)
              break;
            end % ~isempty(waterfile)
          end % for bi
        end % if 
        if ~isempty(waterfile)
          break;
        end % ~isempty(waterfile)
      end % for fi ...
    end

    function res=check_support(op,silent)
      % CHECK_SUPPORT returns capabilities of the current module (also taking into account necessary dependencies)
      %
      %   CHECK_SUPPORT() returns a struct containing "read", "write", "import", "run" and "intermediate" flags
      %
      %   CHECK_SUPPORT(OP) returns a single flag corresponding to the operation specified in OP.
      %
      %   CHECK_SUPPORT(..., SILENT) suppresses output messages when SILENT is non-zero.


      if (nargin<2) silent=0; end;
      if (nargin<1) op=[]; end;

      capabilities=struct('read',0,'write',0,'import',0,'run',0,'intermediate',1);
      % "intermediate" indicates basic functionality (eg, converting to native structure) independent of IO or execution.

      if ~isempty(op)
        res=capabilities.(op)
      else
        res=capabilities;
      end
    end

    function res=can_import(d)
      % CAN_IMPORT returncheck whether this module is able to import data in the supplied format
    end

    function result=check_negative_water(freq,spec)
      % CHECK_NEGATIVE_WATER checks for negative residual water
      %
      % CHECK_NEGATIVE_WATER returns true if data in the vicinity of the residual water is of opposite sign to data in the "typical" metabolite range on FREQ (regardless of whether or not the entire spectrum is inverted)

      met_mask=(freq>2.8) & (freq<4.2);
      h2o_mask=(freq>4.4) & (freq<4.9); % arbitrarily broad water range

      h2o_part=real(spec(h2o_mask));
      met_part=real(spec(met_mask));

      [v,i]=max(abs(h2o_part));
      sign_h2o=sign(h2o_part(i));

      [v,i]=max(abs(met_part));
      sign_met=sign(met_part(i));

      result=sign_h2o~=sign_met;
    end

    function result=check_flipped_metabolite_spectrum(freq,spec)
      % CHECK_FLIPPED_METABOLITE_SPECTRUM checks sign of input spectrum
      %
      % CHECK_FLIPPED_METABOLITE_SPECTRUM(FREQ,SPEC) returns true if data in SPEC is more negative than positive within a "typical" metabolite range on FREQ.

      dbg=0;

      met_mask=(freq>2.8) & (freq<4.2);
      result=max(-real(spec(met_mask)))>max(real(spec(met_mask)));

      if dbg
        size(spec)
        figure();
        plot(freq,spec); hold on; plot(freq(met_mask),spec(met_mask),'r','LineWidth',2);
        MRS_plot.format_freq();
        MRS_plot.gaba_markers();
      end
    end

    function ok=check_on_off_order(freq,data_on,data_off,target)
      % CHECK_ON_OFF_ORDER check for correct on/off order
      %
      % CHECK_ON_OFF_ORDER(FREQ,DATA_ON,DATA_OFF) returns true if DATA_ON-DATA_OFF is more strongly
      % positive than negative in the expected NAA range. This is only meaningful for regular
      % GABA-edited megapress! (in future, TARGET parameter will allow for other edit modes).
      %
      % CHECK_ON_OFF_ORDER(FREQ,DIFF) does the same, but with DIFF externally calculated
      %
      % For obvious reasons, this should be called after first verifying that the spectra are not
      % inverted.
      %
      % See Also CHECK_FLIPPED_METABOLITE_SPECTRUM.

      dbg=0;

      neg_mask=(freq>1.7) & (freq<2.1);
      %result=max(real(data_off(neg_mask)))>max(real(data_on(neg_mask)));
      if nargin>2 && ~isempty(data_off)
        diff=data_on-data_off;
      else
        diff=data_on
      end
      ok=max(-real(diff(neg_mask)))>max(real(diff(neg_mask)));

      if dbg
        size(diff)
        figure();
        plot(freq,diff); hold on; plot(freq(neg_mask),diff(neg_mask),'r','LineWidth',2);
        if ok 
          title('order OK')
        else
          title('INVERTED ON/OFF')
        end
        MRS_plot.format_freq();
        MRS_plot.gaba_markers();
      end
    end

  end
end
