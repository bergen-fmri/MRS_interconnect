classdef MRS_plot
  % MRS_plot support functions for plotting MRS data
  %
  % This class encapsulates a number of basic plot formatting functions, for somewhat consistent display of time- and frequency-domain MRS data.
  %
  % Alexander R. Craven, University of Bergen, 2018-10-17
  
  methods(Static)

    function h=figure()
      % FIGURE creates a somewhat standard-sized figure
      sz=get(0,'ScreenSize');
      h=figure('Position',[sz(3)/5,sz(4)/5,1000,600]);
    end

    function suptitle(txt,txt2)
      % SUPTITLE render title(s) over subplots
      %
      % This function renders a (slightly ugly) title line above a set of subplots;
      %
      % SUPTITLE(txt) renders a large-ish bold title in the upper left corner
      %
      % SUBTITLE(txt,txt2) renders a large-ish bold title in the upper left corner, and a regular-sized title in the upper right.

      txt=MRS_plot.fix_underscores(txt);
      ax1=axes('Position',[0 0 1 1],'Visible','off','HitTest','off');

      % want to specify a pixel offset, but retain normalized units such that resizing etc behaves sensibly...
      sz=getpixelposition(ax1);
      text(5/sz(3),1-(5/sz(4)),txt,'VerticalAlignment','top','HorizontalAlignment','left','Units','normalized','FontWeight','Bold','FontSize',1.2*get(gca,'FontSize'))

      if nargin>=2
        txt2=MRS_plot.fix_underscores(txt2);
        text(1-5/sz(3),1-(5/sz(4)),txt2,'VerticalAlignment','top','HorizontalAlignment','right','Units','normalized','FontWeight','Normal','FontSize',1.0*get(gca,'FontSize'))
      end

    end

    function ppmaxis()
      % PPMAXIS sets up x-axis direction and labelling appropriate for a ppm scale
      set(gca,'Xdir','reverse'); xlabel('ppm');
    end

    function timeaxis()
      % TIMEAXIS sets up x-axis direction and label appropriate for a time scale
      xlabel('time');
      set(gca,'Xdir','normal');
      axis tight;
    end

    function zeroline()
      % ZEROLINE draws a horizontal line across the full scale, at y=0
      xl=get(gca,'Xlim');
      line([min(xl),max(xl)],[0,0],'LineStyle','--','Color',[0.3,0.3,0.3],'HandleVisibility','off');
    end

    function gaba_markers()
      % GABA_MARKERS draws a series of vertical lines at full scale, with x positions corresponding to key points in a typical megapress spectrum.
      yl=get(gca,'Ylim');
      for ppm=[1.9 2.008 3.026 3.7433]
        line([ppm,ppm],[min(yl),max(yl)],'LineStyle',':','Color',[0.7,0.7,0.7],'HandleVisibility','off');
      end
    end

    function metab_scale()
      % METAB_SCALE adjusts y-axis scaling to optimise visibility in a typical metabolite range (1.0-4.2 ppm)
      %
      % This is useful in cases with strong residual water.

      allvals=[];
      h = findobj(gca,'Type','line');
      x=get(h,'Xdata');
      y=get(h,'Ydata');
      if length(h)==1
        % multi-line data is returned as a cell array of vectors, whilst single line data is simply returned as a matrix. we need consistent behaviour, so put the single-row data into a cell array too.
        x={x};
        y={y};
      end
      for i=1:length(h)
        if length(x{i})>2
          mask=(x{i} > 1.0) & (x{i} < 4.2);
          vals=y{i}(mask);
          if length(vals)>0
            allvals=[allvals; vals(:)];
          end
        end
      end
      if length(allvals)>10 % only bother if we have a non-trivial number of points to work with.
        sorted=sort(allvals);
        ix=[ 1+floor(0.005*length(sorted-1)), ceil(0.995*length(sorted)) ];
        range=sorted(ix(2))-sorted(ix(1));
        scale=sorted(ix)+[-.15; .15].*range;
        set(gca,'Ylim',scale);
      end
    end

    function format_freq()
      % FORMAT_FREQ applies standard formatting for a frequency-domain plot
      %
      % See Also FORMAT_TIME PPMAXIS ZEROLINE

      MRS_plot.ppmaxis();

      % add legend in case of multiple plots
      num_subplots=length(findall(gca,'type','line')); % ish. lines confuse it a bit.
      if (num_subplots>1 && num_subplots<15) % why the upper limit? some of our figures have each average as a separate plot line, making for a particularly unwieldy legend
        yl=get(gca,'Ylim');
        if -min(yl)>max(yl) % where shall we put it?
          legend('Location','southwest')
        else
          legend('Location','northwest')
        end
        legend('boxoff')
      end

      MRS_plot.zeroline();
    end

    function format_time()
      % FORMAT_TIME applies standard formatting for a time-domain plot
      %
      % See Also FORMAT_FREQ TIMEAXIS ZEROLINE

      MRS_plot.timeaxis();

      % add legend in case of multiple plots
      num_subplots=length(findall(gca,'type','line')); % ish. lines confuse it a bit.
      if (num_subplots>1 && num_subplots<15)
        legend('Location','northeast')
        legend('boxoff')
      end

      MRS_plot.zeroline();
    end

    function export(filename,formats)
      % EXPORT saves current figure in the specified format(s)
      %
      % By default, images are exported both in vector (.fig) and raster (.png)
      % formats for convenience.
      %
      %   EXPORT(FILENAME) exports to a file derived from FILENAME, with format-appropriate suffix
      %
      %   EXPORT(FILENAME,FORMATS) does the same, but using user-specified file formats instead of the default (.png, .fig)
      %
      % See Also saveas print

      [dirname,basename,ext]=fileparts(filename);
      if (nargin<2)
        formats={'.png','.fig'};
      end
      if length(ext)>0 && ~any(cellfun(@(x) strcmp(x,ext),formats))
        formats{end+1}=ext;
      end
      for i=1:length(formats)
        ext=formats{i};
        if strcmp(ext,'.fig')
          saveas(gcf,fullfile(dirname,[basename ext]));
        else
          driver=ext(2:end);
          print(fullfile(dirname,[basename ext]),['-d' driver],'-r0');
        end
      end
    end

    function str=fix_underscores(str)
      % FIX_UNDERSCORES fixes underscores in text strings for figure labels.
      %
      % This function alters the Matlab standard underscore behaviour for
      % figure titles and labels into something a little less irritating --
      % allowing single, isolated underscores to be rendered as simple
      % underscores, whilst retaining the possibility of using Tex-esque
      % formatting if desired.
      %
      % A single underscore (a_b) will be escaped, yielding a\_b (which is rendered as an underscore rather than a subscript)
      %
      % A double underscore (a__b) will be converted to a single underscore, yielding a_b (which is rendered as a single-character subscript)
      %
      % A single underscore followed by a curly brace (a_{b}) will be unchanged; this will be rendered as a subscript.
      %
      % See Also, <a href="matlab:doc text_props">doc text_props</a> (Interpreter) and <a href="matlab:doc title">doc title</a>

      str=regexprep(str,'([^\\_])_([^_{])','$1\\_$2'); % replace single underscores (not associated with {}) with \_
      str=regexprep(str,'([^\\_])__[^_]','$1_');       % replace double underscores with single ones.
    end
  end % methods(Static)
end % classdef
