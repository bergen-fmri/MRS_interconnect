function statusbar(txt)
  % STATUSBAR minimal desktop status-bar setting function
  %
  %     STATUSBAR(TXT) sets the desktop status bar to TXT.
  %
  %     STATUSBAR() or STATUSBAR('') clears the status bar.
  %
  % If no desktop frame can be found, TXT is displayed instead.
  %
  %
  % Implemented with reference to:
  %     http://undocumentedmatlab.com/blog/setting-status-bar-text
  %
  % A more versatile, complete implementation is available in the Matlab File
  % Exchange, here:
  %
  %   https://se.mathworks.com/matlabcentral/fileexchange/14773-statusbar
  %
  % Alexander R. Craven, University of Bergen, 2018-10-17

  if nargin==0 || isempty(txt)
    % Clear the status bar. Note that neither an empty string nor single space
    % achieve this, but two spaces apparently do...
    txt='  ';
  end
  ok=0;
  try
    desktop=com.mathworks.mde.desk.MLDesktop.getInstance;
    if desktop.hasMainFrame
      % nee
      t=timer('TimerFcn',{@setStatusText,desktop,txt}, 'StartDelay',0.05, 'ExecutionMode','singleShot');
      start(t);
      ok=1;
    end
  catch
  end
  if ~ok && ~isempty(strtrim(txt))
    disp(txt);
  end
end

function setStatusText(varargin)
  targ=varargin{3};
  txt=varargin{4};
  targ.setStatusText(txt);
end
