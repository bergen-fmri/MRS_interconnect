classdef MRS_interconnect_FIDA < MRS_interconnect_base
  % MRS_interconnect_FIDA : Import/Output and Preprocessing using FID-A functions
  %
  %---------------------------------------------------------
  %  2018-09-20  Alexander R. Craven (University of Bergen)
  %---------------------------------------------------------
  % Significant portions of this implementation originate from FID-A's run_megapressproc_auto example script
  % https://github.com/CIC-methods/FID-A/blob/master/exampleRunScripts/run_megapressproc_auto.m
  % Jamie Near, McGill University, 2014 (2017 for the GE variant)
  

  properties (Access=protected)
    fn
    raw
    out_cc
    outw_cc
    water
  end

  methods % {{{

    function self=MRS_interconnect_FIDA(d, varargin)
      self=self@MRS_interconnect_base(d,varargin{:}); % fall back on default constructor
    end

    function res=import(self,d)
      % IMPORT import data using FID-A I/O routines
      %
      % Beware, this implementation has known limitations.

      dbg=0;
      if ischar(d) && (exist(d,'file') || exist(d,'dir'))

        if (exist(d,'dir'))
          % find twix data...
          [filename]=dir(fullfile(d,'*.dat'));
          d=filename.name(1:end)
        end

        [dirname,basename,ext]=fileparts(d);

        waterfile=[];

        if (strcmp(ext,'.7') && exist('io_loadspec_GE') && exist(d,'file'))
          vendor='GE';
          warning('FID-A I/O as used here provides limited header processing abilities... making various unfounded assumptions about the data type.');
          [met,ref]=io_loadspec_GE(d,5000,127.7,2,68,2000);
          self.log(['io_loadspec_GE ' d ' (using assumed parameters! I''d strongly recommend using GannetLoad instead... perhaps via MRS_interconnect_Gannet)'])
        elseif strcmpi(ext,'.dat') && exist('io_loadspec_twix')
          vendor='Siemens_twix';

          met=io_loadspec_twix(d);
          self.log(['io_loadspec_twix ' d])

          waterfile=MRS_interconnect_base.find_paired_reference(d);

          if ~isempty(waterfile)
            self.water=1;
            ref=io_loadspec_twix(waterfile);
            self.log(['io_loadspec_twix ' waterfile])
          else
            self.water=0;
            ref=[];
          end
        elseif strcmpi(ext,'.sdat')&&exist('io_loadspec_sdat')
          vendor='Philips';
          nsub=2;

          try
            met=io_loadspec_sdat(d,nsub);
          catch Ex
            if strcmp(Ex.identifier,'MATLAB:textread:FileNotFound') && strcmp(ext,'.SDAT') && exist(d,'file')
              warning('Some FID-A versions have trouble with case-sensitive filenames! This may explain your current affliction; please fix sparname in philipsLoad, see stack below...');
            end
            rethrow(Ex);
            %if 
            %end
          end

          self.log(['io_loadspec_sdat ' d ' (using assumed value for subspecs param!)'])

          waterfile=MRS_interconnect_base.find_paired_reference(d);

          if ~isempty(waterfile)
            self.water=1;
            ref=io_loadspec_sdat(waterfile,nsub);
            self.log(['io_loadspec_sdat' waterfile])
          else
            self.water=0;
            ref=[];
          end
        else 
          error('Unsupported file format.');
        end

        % philips: data/list  io_loadspec_data
        % OR io_loadspec_sdat  sdat/spar

        self.fn=d;

        % MRS_struct.p.GE ?
        % missing: p.voxdim, p.nrows,
        % p.zf

        if strcmp(vendor,'GE')
          met=op_complexConj(met);
        end

        if isstruct(ref)
          self.water=1;
          if strcmp(vendor,'GE')
            ref=op_complexConj(ref);
          end
        else
          self.water=0;
        end

        MRS_struct=MRS_interconnect_FIDA.to_MRS_struct(met,ref);

        MRS_struct.metabfile={d};
        MRS_struct.p.vendor=vendor;

        if (met.dims.coils==0 || met.flags.addedrcvrs) % data do not contain multiple receivers?
          self.log('op_getcoilcombos skipped: source data does not appear to contain multiple receivers');
          out_cc=met;
          outw_cc=ref;
        else
          % this section is borrowed from Jamie Near's FID-A run_megapressproc_GEauto.m {{{
          %first step should be to combine coil channels.  To do this find the coil
          %phases from the water unsuppressed data.
          if self.water
            coilcombos=op_getcoilcombos(ref,1);
            [outw_cc,fidw_pre,specw_pre,phw,sigw]=op_addrcvrs(ref,1,'w',coilcombos);
            self.log('op_getcoilcombos using water reference');
          else
            coilcombos=op_getcoilcombos(op_averaging(op_combinesubspecs(met,'summ')),1);
            self.log('op_getcoilcombos from metab spectrum');
          end

          [out_cc,fid_pre,spec_pre,ph,sig]=op_addrcvrs(met,1,'w',coilcombos);
        end

        % }}}

        scalefactor=1.0;
        mult_metab=1.0;
        mult_water=1.0;

        if strcmp(vendor,'GE')
          % io_loadspec_GE mis-reports averages/rawAverages in the water reference
          outw_cc.averages=outw_cc.sz(outw_cc.dims.averages);
          outw_cc.rawAverages=outw_cc.averages;
          % with FID-A import, we don't get info on nex or noadd... so instead, we need to make a few more guesses and assumptions...
          noadd=outw_cc.averages>=8;
          nex=8; % default is 8, but could be 2
          % GERead: mult, multw (in Gannet no-add, multw=mult*nechoes)
          if noadd
            mult_metab=nex/4;
            mult_water=nex/2;
          end
        end

        if strcmp(vendor,'GE')
          self.log('GE-specific: doing global downscale, for consistency with Gannet (see bottom of GERead.m)');
          % GE: global rescale (for consistency with Gannet)
          scalefactor=scalefactor/MRS_interconnect_config.get('global_downscale_ge_gannet',1);
        end

        nechoes=out_cc.subspecs;
        dataframes=out_cc.averages/nechoes;

        if scalefactor~=1.0
          out_cc.fids=out_cc.fids.*scalefactor.*mult_metab;
          out_cc.specs=out_cc.specs.*scalefactor.*mult_metab;
          outw_cc.fids=outw_cc.fids.*scalefactor.*mult_water;
          outw_cc.specs=outw_cc.specs.*scalefactor.*mult_water;
        end

        %refframes=outw_cc.sz(outw_cc.dims.averages) % gannet GERead divides this by number of echoes...

        I1=kron(1:dataframes,ones(1,nechoes))';
        I2=repmat(1:nechoes,1,dataframes)';
        I1+out_cc.sz(2)*(I2-1);
        ONOFF=I2-1; % FIXME make sure this agrees with ONOFForder
        f=reshape(out_cc.fids,out_cc.sz(1),out_cc.sz(2)*out_cc.sz(3));
        FullData=f(:,I1+out_cc.sz(2)*(I2-1));

        if self.water
          nechoes=outw_cc.subspecs;
          refframes=outw_cc.averages/nechoes;
          I1=kron(1:refframes,ones(1,nechoes))';
          I2=repmat(1:nechoes,1,refframes)';
          I1+outw_cc.sz(2)*(I2-1);

          f=reshape(outw_cc.fids,outw_cc.sz(1),nechoes*refframes);
          WaterData=f(:,I1+outw_cc.sz(2)*(I2-1));
        end

        if dbg
          size(FullData)
          size(WaterData)


          figure();
          size(FullData)
          imagesc(imag(FullData))
          figure();
          plot(real(FullData(4,:)))
        end

        MRS_struct.fids.data=conj(FullData);
        if self.water
          MRS_struct.fids.data_water=conj(WaterData);
        end
        MRS_struct.fids.ON_OFF=ONOFF;

        MRS_struct.spec.vox1=struct();
        if self.water
          MRS_struct.spec.vox1.water=fftshift(fft(mean(MRS_struct.fids.data_water,2)));
        end


        res=MRS_struct;
        self.MRS_struct=MRS_struct;

        self.out_cc=out_cc;

        self.preprocess(); % continue with default FID-A preprocessing
        
      end
    end;

    function res=preprocess(self,coilcombos,avgAlignDomain,alignSS) % {{{
      % PREPROCESS processes raw data using FID-A routines
      %
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % This function is derived from FID-A's run_megapressproc_auto example script                 %
      % https://github.com/CIC-methods/FID-A/blob/master/exampleRunScripts/run_megapressproc_auto.m %
      % Jamie Near, McGill University, 2014 (2017 for the GE variant)                               %
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      dbg=0;
      self.log_provenance('Proc');

      if nargin<4
          alignSS=2;
          if nargin<3
              avgAlignDomain='f';
              if nargin<2
                  ccGiven=false;
              else    
                  ccGiven=true;
              end     
          end
      end

      [out_cc,outw_cc]=self.from_MRS_struct(self.MRS_struct);

      if ~isempty(outw_cc)
        self.water=1;
      elseif self.water
        error('Water reference got lost along the way :(');
      end

      % If this object was constructed from an existing MRS_struct, the recorded freq scale may include zero filling from Gannet. Since we're re-processing now, overwrite this with a scale suitable for unfilled data.
      self.MRS_struct.spec.freq=out_cc.ppm;

      vendor=self.MRS_struct.p.vendor;


      interactive=0;

      %%%%%%%%OPTIONAL REMOVAL OF BAD AVERAGES%%%%%%%%%%%%%%%%%%%% {{{
      close all;
      out_cc2=out_cc;
      nBadAvgTotal=0;
      nbadAverages=1;
      rmbadav='y';
      close all;

      plot_xlim_broad=[0.2 5.2];
      plot_xlim_tight=[1.0 4.3];

      nsubspecs=out_cc.sz(out_cc.dims.subSpecs);

      if rmbadav=='n' || rmbadav=='N'
          out_rm=out_cc;
          nsd='N/A';
      else
          sat='n';
          while sat=='n' || sat=='N'
              nsd=4; %Setting the number of standard deviations;
              iter=1;
              nbadAverages=1;
              nBadAvgTotal=0;
              out_cc2=out_cc;
              while nbadAverages>0
                  [out_rm,metric{iter},badAverages]=op_rmbadaverages(out_cc2,nsd,'t');
                  badAverages;
                  nbadAverages=length(badAverages)*nsubspecs;
                  nBadAvgTotal=nBadAvgTotal+nbadAverages;
                  out_cc2=out_rm;
                  iter=iter+1;
                  disp([num2str(nbadAverages) ' bad averages removed on this iteration.']);
                  disp([num2str(nBadAvgTotal) ' bad averages removed in total.']);
                  close all;
              end

              self.log(sprintf('Removed %d bad averages (after %d iterations)', nBadAvgTotal, iter-1))

              %figure('position',[0 50 560 420]);
              %Make figure to show pre-post removal of averages
              h=figure('visible','off');
              subplot(2,2,1);
              plot(out_cc.ppm,real(out_cc.specs(:,:,1)));xlim(plot_xlim_tight);
              set(gca,'FontSize',8);
              set(gca,'XDir','reverse');
              xlabel('Frequency (ppm)','FontSize',10);
              ylabel('Amplitude(a.u.)','FontSize',10);
              title('Edit-ON Before','FontSize',12);
              box off;
              subplot(2,2,2);
              plot(out_rm.ppm,real(out_rm.specs(:,:,1)));xlim(plot_xlim_tight);
              set(gca,'FontSize',8);
              set(gca,'XDir','reverse');
              xlabel('Frequency (ppm)','FontSize',10);
              ylabel('Amplitude(a.u.)','FontSize',10);
              title('Edit-ON After','FontSize',12);
              box off;
              subplot(2,2,3);
              plot(out_cc.ppm,real(out_cc.specs(:,:,2)));xlim(plot_xlim_tight);
              set(gca,'FontSize',8);
              set(gca,'XDir','reverse');
              xlabel('Frequency (ppm)','FontSize',10);
              ylabel('Amplitude(a.u.)','FontSize',10);
              title('Edit-OFF Before','FontSize',12);
              box off;
              subplot(2,2,4);
              plot(out_rm.ppm,real(out_rm.specs(:,:,2)));xlim(plot_xlim_tight);
              set(gca,'FontSize',8);
              set(gca,'XDir','reverse');
              xlabel('Frequency (ppm)','FontSize',10);
              ylabel('Amplitude(a.u.)','FontSize',10);
              title('Edit-OFF After','FontSize',12);
              box off;
              set(h,'PaperUnits','centimeters');
              set(h,'PaperPosition',[0 0 20 15]);
              saveas(h,self.get_filename('_01_rmBadAvg_prePostFig.png'))
              saveas(h,self.get_filename('_01_rmBadAvg_prePostFig.fig'))
              close(h);

              %figure('position',[0 550 560 420]);
              h=figure('visible','off');
              plot([1:length(metric{1})],metric{1},'.r',[1:length(metric{iter-1})],metric{iter-1},'x','MarkerSize',16);
              set(gca,'FontSize',8);
              xlabel('Scan Number','FontSize',10);
              ylabel('Deviation Metric','FontSize',10);
              legend('Before rmBadAv','Before rmBadAv','After rmBadAv','After rmBadAv');
              legend boxoff;
              title('Deviation Metric','FontSize',12);
              box off;
              set(h,'PaperUnits','centimeters');
              set(h,'PaperPosition',[0 0 20 10]);
              saveas(h,self.get_filename('_01_rmBadAvg_scatterFig.png'))
              saveas(h,self.get_filename('_01_rmBadAvg_scatterFig.fig'))
              close(h);

              %sat1=input('are you satisfied with the removal of bad averages? ','s');
              sat='y';

          end
      end

      % }}}

      %NOW ALIGN AVERAGES:  A.K.A. Frequency Drift Correction.  {{{
      driftCorr='y';
      if driftCorr=='n' || driftCorr=='N'
          out_av=op_averaging(out_rm);
          if self.water
              outw_av=op_averaging(outw_cc);
          end
          fs=0;
          phs=0;
      else
          if self.water
            if outw_cc.averages>1
              outw_aa=op_alignAverages(outw_cc,0.2,'n');
            else
              self.log('Only one water frame in source; skipping op_alignAverages','WARN');
              outw_aa=[];
              outw_av=outw_cc;
            end
          end
          sat='n';
          out_rm2=out_rm;
          while sat=='n' || sat=='N'
              iter=0;
              iterin=20;
              p=100;
              fscum=zeros(out_rm.sz(2:end));
              phscum=zeros(out_rm.sz(2:end));
              while (abs(p(1))>0.0003 && iter<iterin)
                  iter=iter+1;
                  close all
                  tmax=0.25+0.03*randn(1);
                  ppmmin=1.6+0.1*randn(1);
                  ppmmaxarray=[3.5+0.1*randn(1,2),4+0.1*randn(1,3),5.5+0.1*randn(1,1)];
                  ppmmax=ppmmaxarray(randi(6,1));
                  switch avgAlignDomain
                      case 't'
                          [out_aa,fs,phs]=op_alignAverages(out_rm2,tmax,'y');
                      case 'f'
                          [out_aa,fs,phs]=op_alignAverages_fd(out_rm2,ppmmin,ppmmax,tmax,'y');
                      otherwise
                          error('ERROR: avgAlignDomain not recognized!');
                  end

                  x=repmat([1:size(fs,1)]',1,out_aa.sz(out_aa.dims.subSpecs));
                  p=polyfit(x,fs,1);
                  disp(sprintf('iteration %d, p: %s',iter,num2str(p)))

                  fscum=fscum+fs;
                  phscum=phscum+phs;

                  if driftCorr=='y' || driftCorr=='Y'
                      out_rm2=out_aa;
                  end
              end

              h=figure('visible','off');
              subplot(2,2,1);
              plot(out_rm.ppm,real(out_rm.specs(:,:,1)));xlim(plot_xlim_tight);
              set(gca,'FontSize',8);
              set(gca,'XDir','reverse');
              xlabel('Frequency (ppm)','FontSize',10);
              ylabel('Amplitude(a.u.)','FontSize',10);
              title('Edit-ON Before','FontSize',12);
              box off;
              subplot(2,2,2);
              plot(out_aa.ppm,real(out_aa.specs(:,:,1)));xlim(plot_xlim_tight);
              set(gca,'FontSize',8);
              set(gca,'XDir','reverse');
              xlabel('Frequency (ppm)','FontSize',10);
              ylabel('Amplitude(a.u.)','FontSize',10);
              title('Edit-ON After','FontSize',12);
              box off;
              subplot(2,2,3);
              plot(out_rm.ppm,real(out_rm.specs(:,:,2)));xlim(plot_xlim_tight);
              set(gca,'FontSize',8);
              set(gca,'XDir','reverse');
              xlabel('Frequency (ppm)','FontSize',10);
              ylabel('Amplitude(a.u.)','FontSize',10);
              title('Edit-OFF Before','FontSize',12);
              box off;
              subplot(2,2,4);
              plot(out_aa.ppm,real(out_aa.specs(:,:,2)));xlim(plot_xlim_tight);
              set(gca,'FontSize',8);
              set(gca,'XDir','reverse');
              xlabel('Frequency (ppm)','FontSize',10);
              ylabel('Amplitude(a.u.)','FontSize',10);
              title('Edit-OFF After','FontSize',12);
              box off;
              set(h,'PaperUnits','centimeters');
              set(h,'PaperPosition',[0 0 20 15]);
              saveas(h,self.get_filename('_02_alignAvgs_prePostFig.png'))
              saveas(h,self.get_filename('_02_alignAvgs_prePostFig.fig'))

              close(h);

              h=figure('visible','off');
              plot([1:out_aa.sz(out_aa.dims.averages)],fscum,'.-','LineWidth',2);
              set(gca,'FontSize',8);
              xlabel('Scan Number','FontSize',10);
              ylabel('Frequency Drift [Hz]','FontSize',10);
              box off;
              legend('Edit-on scans','Edit-off scans','Location','SouthEast');
              legend boxoff;
              title('Estimated Freqeuncy Drift','FontSize',12);
              set(h,'PaperUnits','centimeters');
              set(h,'PaperPosition',[0 0 10 10]);
              saveas(h,self.get_filename('_03_freqDriftFig.png'))
              saveas(h,self.get_filename('_03_freqDriftFig.fig'))
              close(h);

              h=figure('visible','off');
              plot([1:out_aa.sz(out_aa.dims.averages)],phscum,'.-','LineWidth',2);
              set(gca,'FontSize',8);
              xlabel('Scan Number','FontSize',10);
              ylabel('Phase Drift [Deg.]','FontSize',10);
              box off;
              legend('Edit-on scans','Edit-off scans');
              legend boxoff;
              title('Estimated Phase Drift','FontSize',12);
              set(h,'PaperUnits','centimeters');
              set(h,'PaperPosition',[0 0 10 10]);
              saveas(h,self.get_filename('_03_phaseDriftFig.png'))
              saveas(h,self.get_filename('_03_phaseDriftFig.fig'))
              close(h);
              sat='y';
              if sat=='n'
                  iter=0;
                  p=100;
                  fscum=zeros(out_rm.sz(2:end));
                  phscum=zeros(out_rm.sz(2:end));
                  fs2cum=zeros(out_cc.sz(2:end));
                  phs2cum=zeros(out_cc.sz(2:end));
                  out_rm2=out_rm;
                  out_cc2=out_cc;
              end
              totalFreqDrift=mean(max(fscum)-min(fscum));
              totalPhaseDrift=mean(max(phscum)-min(phscum));
              close all
          end
          %now combine the averages averages
          if dbg
            disp('Before combining averages:')
            out_aa
          end
          out_av=op_averaging(out_aa);
          if dbg
            disp('After combining averages:')
            out_av
          end
          if self.water && ~isempty(outw_aa)
            if dbg
              disp('Water before combining:')
              outw_aa
            end
            outw_av=op_averaging(outw_aa);
            if dbg
              disp('Water after combining:')
              outw_av
            end
          end
      end % align averages }}}

      self.log(sprintf('Align averages; freq drift %.2f phase drift %.2f after %d iterations',totalFreqDrift,totalPhaseDrift,iter))

      %now leftshift {{{
      out_ls=op_leftshift(out_av,out_av.pointsToLeftshift);
      if self.water
          outw_ls=op_leftshift(outw_av,outw_av.pointsToLeftshift);
      end

      self.log(sprintf('Left shift by %d points',out_av.pointsToLeftshift));
      % }}}

      out_ls_ss2=op_takesubspec(out_ls,2);
      if strcmp(vendor,'GE')
        %now do automatic zero-order phase correction (Use water Peak):
        %SpecTool(out_ls_ss2,0.2,1,3.5);
        [out_ls_ss2_ph,ph0]=op_autophase(out_ls_ss2,4,5.5);
        self.log(sprintf('Metab: Zero-order phase correction using water peak; ph0=%0.2f',ph0))
      else
        %now do automatic zero-order phase correction (Use Creatine Peak):
        [out_ls_ss2_ph,ph0]=op_autophase(out_ls_ss2,2.9,3.1)
        self.log(sprintf('Metab: Zero-order phase correction using creatine peak; ph0=%0.2f',ph0))
      end
      %ph0=input('Input zero-order phase:');
      out_ph=op_addphase(out_ls,ph0);
      if self.water
          outw_ph=op_addphase(outw_ls,ph0);
          self.log(sprintf('Water: Add phase ph0=%.2f',ph0))
      end

      %Now align subspecs if desired: {{{
      switch alignSS

          case 2
              out=op_alignMPSubspecs(out_ph);
              self.log('op_alignMPSubspecs');

      %         figure('position',[0 50 560 420]);
      %         out_ph_filt=op_filter(out_ph,5);
      %         subSpecTool(out_ph_filt,0,7);
      %         disp('***************************************************************************************');
      %         disp('Use GUI interface to align edit-ON and edit-OFF scans by adjusting Phase and Frequency.');
      %         disp('Try to minimize the residual water, residual Creatine, and residual Choline peaks!');
      %         disp('***NOTE If you are using the Siemens MEGA_PRESS WIP (WIP529), then you will');
      %         disp('have to add about 180 degrees of phase to the subspectrum!***');
      %         disp('*************************************************************');
      %         fprintf('\n');
      %         phshft1=input('Input Desired Phase Shift (Degrees) for first spectrum: ');
      %         frqshft1=input('Input Desired Frequncy Shift (Hz) for first spectrum: ');
      %         out=op_freqshiftSubspec(op_addphaseSubspec(out_ph,phshft1),frqshft1);
      %         close all;

          case 0
              out=out_ph;

          otherwise
              error('ERROR: alignSS value not valid! ');
      end % align subspecs }}}

      %Make fully processed data;
      diffSpec=op_combinesubspecs(out,'diff');
      sumSpec=op_combinesubspecs(out,'summ');
      subSpec1=op_takesubspec(out,1);
      if strcmp(vendor,'Siemens_twix')
        subSpec1=op_addphase(subSpec1,180);
        self.log('Siemens_twix-specific: op_addphase(subSpec1,180)');
      end
      if strcmp(vendor,'Philips')
        subSpec1=op_addphase(subSpec1,180);
        self.log('Philips-specific: op_addphase(subSpec1,180)');
      end
      subSpec2=op_takesubspec(out,2);
      if strcmp(vendor,'GE')
        subSpec2=op_addphase(subSpec2,180);
        self.log('GE-specific: op_addphase(subSpec2,180)');
      end

      % now, check that we got everything right above:

      if MRS_interconnect_config.get('do_automatic_corrections',1)

        if self.check_negative_water(subSpec1.ppm,subSpec1.specs) || self.check_negative_water(subSpec2.ppm,subSpec2.specs)
          self.log('Residual water in these spectra may be inverted.');
        end

        if self.check_flipped_metabolite_spectrum(subSpec1.ppm,subSpec1.specs)
          self.log('Metabolite subSpec1 appears to be inverted; adding 180 deg phase.','WARN');
          subSpec1=op_addphase(subSpec1,180);
        end

        if self.check_flipped_metabolite_spectrum(subSpec2.ppm,subSpec2.specs)
          self.log('Metabolite subSpec2 appears to be inverted; adding 180 deg phase.','WARN');
          subSpec2=op_addphase(subSpec2,180);
        end

        if ~self.check_on_off_order(subSpec1.ppm,subSpec1.specs,subSpec2.specs)
          self.log('ON/OFF order appears to be incorrect for this dataset; swapping.','WARN');
          x=subSpec2;
          subSpec2=subSpec1;
          subSpec1=x;
          diffSpec=op_addphase(diffSpec,180);
        end

      end


      %Frequency shift all spectra so that Creatine appears at 3.027 ppm:
      [subSpec1,frqShift]=op_ppmref(subSpec1,2.9,3.1,3.027);
      diffSpec=op_freqshift(diffSpec,frqShift);
      sumSpec=op_freqshift(sumSpec,frqShift);
      subSpec2=op_freqshift(subSpec2,frqShift);


      %Make final water unsuppressed data {{{
      % run_megapressproc_auto uses outw_ls, run_megapress_GEauto uses outw_ph
      if self.water
          if ~isempty(findstr(outw_ph.seq,'edit_529')) || ~isempty(findstr(outw_ph.seq,'jn_svs_special'))
              if outw_ph.dims.subSpecs
                  outw=op_combinesubspecs(outw_ph,'diff');
                  self.log([outw_ph.seq, ': water : op_combinesubspecs(outw_ph,diff)']);
              else
                  outw=outw_ph;
                  self.log([outw_ph.seq, ': water : no subSpecs; outw=outw_ph']);
              end
          else
              if outw_ph.dims.subSpecs
                  outw=op_combinesubspecs(outw_ph,'summ');
                  self.log('water : op_combinesubspecs(outw_ph,summ)');
              else
                  outw=outw_ph;
                  self.log('water : no subSpecs; outw=outw_ph');
              end
          end
          outw=op_addphase(outw,-phase(outw.fids(1))*180/pi,0,4.65,1);
      else
          outw=0;
      end % }}}

      v1=self.MRS_struct.p.Vox{1};
      t1=self.MRS_struct.p.target;

      % FID-A uses a fftshift(ifft(...)) pattern, which results in conjugate-flipped spectra when compared with Gannet/LCModel preprocessing
      % hence, need to take conj of subSpec1, subSpec2, diffSpec
      self.MRS_struct.spec.(v1).(t1).on=fftshift(fft(conj(subSpec1.fids),[],subSpec1.dims.t),subSpec1.dims.t);
      self.MRS_struct.spec.(v1).(t1).off=fftshift(fft(conj(subSpec2.fids),[],subSpec2.dims.t),subSpec2.dims.t);
      self.MRS_struct.spec.(v1).(t1).diff=fftshift(fft(conj(diffSpec.fids),[],diffSpec.dims.t),diffSpec.dims.t);
      if self.water
        self.MRS_struct.spec.(v1).water=fftshift(fft(conj(outw.fids),[],outw.dims.t),outw.dims.t);
      end



      %Make figure to show the final spectrum:
      h=figure('visible','off');
      subplot(1,2,1);
      plot(subSpec1.ppm,real(subSpec1.specs),subSpec2.ppm,real(subSpec2.specs),'linewidth',2);xlim(plot_xlim_broad);

      % recalculate y-axis scaling on limited metab range, such that it is not dominated by residual water
      mask=subSpec1.ppm<max(plot_xlim_tight) & subSpec1.ppm>min(plot_xlim_tight);
      ymax=max(max(real(subSpec1.specs(mask))),max(real(subSpec2.specs(mask))));
      ymin=min(min(real(subSpec1.specs(mask))),min(real(subSpec2.specs(mask))));
      yrange=[ymin,ymax]+[-0.05,0.2]*(ymax-ymin);

      ylim(yrange);

      set(gca,'FontSize',8);
      set(gca,'XDir','reverse');
      xlabel('Frequency (ppm)','FontSize',10);
      ylabel('Amplitude(a.u.)','FontSize',10);
      legend('Edit-ON','Edit-OFF');
      legend boxoff;
      box off;
      title('Result: Subspecs','FontSize',12);
      subplot(1,2,2);
      plot(diffSpec.ppm,real(diffSpec.specs),'linewidth',2);xlim(plot_xlim_broad);
      set(gca,'FontSize',8);
      set(gca,'XDir','reverse');
      xlabel('Frequency (ppm)','FontSize',10);
      ylabel('Amplitude(a.u.)','FontSize',10);
      legend('diff');
      legend boxoff;
      box off;
      title('Result: Diff Spectrum','FontSize',12);
      set(h,'PaperUnits','centimeters');
      set(h,'PaperPosition',[0 0 20 10]);
      saveas(h,self.get_filename('_05_finalSpecFig.png'))
      saveas(h,self.get_filename('_05_finalSpecFig.fig'))

    end % preprocess }}}

  end % methods }}}

  methods(Static)

    function [met_cc,ref_cc,preprocessed]=from_MRS_struct(MRS_struct)
      % from_MRS_struct returns a FID-A compatible data structure, which may subsequently be passed to FID-A io or processing functions

      if (isfield(MRS_struct.p,'GE') && isfield(MRS_struct.p.GE,'txt_seriesdesc')) % Bergen local : sequence description from p.GE.txt_seriesdesc
        seq=MRS_struct.p.GE.txt_seriesdesc;
      else
        % Construct some sort of sequence name
        if MRS_struct.p.HERMES>0
          seq=[MRS_struct.p.target ',' MRS_struct.p.target2 ' HERMES'];
        else
          seq=MRS_struct.p.target;
        end
        if MRS_struct.p.PRIAM>0
          seq=seq+' PRIAM';
        end

        if MRS_struct.p.phantom>0
          seq=seq+ '(phantom)';
        end
      end

      dims.t=1;
      dims.coils=0;
      dims.averages=2;
      dims.subSpecs=3;
      dims.extras=0;
      s.dims=dims;

      s.fids(:,:,1)=conj(MRS_struct.fids.data(:,MRS_struct.fids.ON_OFF>0)); % FID-A s.fids and LCModel MRS_struct.fids are conjugate-flipped wrt one another
      s.fids(:,:,2)=conj(MRS_struct.fids.data(:,MRS_struct.fids.ON_OFF==0));

      s.specs=fftshift(ifft(s.fids,[],s.dims.t),s.dims.t); % FID-A uses a fftshift(ifft(...)) pattern, which also results in conjugate-flipped spectra when compared with Gannet/LCModel preprocessing

      s.te=MRS_struct.p.TE;
      s.tr=MRS_struct.p.TR;
      s.averages=MRS_struct.p.nrows;
      s.rawAverages=MRS_struct.p.Navg;

      s.spectralwidth=MRS_struct.p.sw;

      vectorsize=size(s.fids,s.dims.t);
      s.sz=size(s.fids); %s.sz=[vectorsize 1 1 1 ];
      s.Bo=MRS_struct.p.LarmorFreq/42.577;
      s.dwelltime=1/s.spectralwidth; % DELTAT, recip of bandwidth

      s.ppm=MRS_struct.spec.freq;
      if length(s.ppm)>s.sz(s.dims.t)
        % MRS_struct.spec.freq includes zero-padding; need to re-create a reasonable un-padded ppm scale
        s.ppm=linspace(s.ppm(1),s.ppm(end),s.sz(s.dims.t));
      end

      s.t=[0:s.dwelltime:(vectorsize-1)*s.dwelltime];

      s.linewidth=MRS_struct.p.LB;
      s.n=MRS_struct.p.npoints;

      s.txfrq=1e6*MRS_struct.p.LarmorFreq; % HZPPM=MRS_struct.p.LarmorFreq

      s.date=(date);
      s.seq=seq;

      s.subspecs=size(s.fids,s.dims.subSpecs);
      s.rawSubspecs=s.subspecs;

      s.flags.writtentostruct=1;
      s.flags.gotparams=1;
      s.flags.leftshifted=0;
      s.flags.filtered=0;
      s.flags.zeropadded=0;
      s.flags.freqcorrected=0;
      s.flags.phasecorrected=0;
      s.flags.averaged=0;
      s.flags.addedrcvrs=1;
      s.flags.subtracted=0;
      s.flags.writtentotext=0;
      s.flags.downsampled=0;
      s.flags.isISIS=0;
      s.pointsToLeftshift=0;
      s.sim=[];

      if (nargout>=1)
        met_cc=s;
      end

      if (nargout>=2) % water reference has been requested
        if isfield(MRS_struct.fids,'data_water')
          s.fids=conj(MRS_struct.fids.data_water);
          % Single-average data sometimes ends up in a row vector... try to clean up the resulting mess:
          if ndims(s.fids)==2 && size(s.fids,1)==1
            s.fids=s.fids';
            s.dims.t=1;
            s.dims.averages=0;
            s.dims.coils=0;
            s.dims.subSpecs=0;
            s.dims.extras=0;
          end;
          s.specs=fftshift(ifft(s.fids,[],s.dims.t),s.dims.t); % FID-A uses a peculiar fftshift(ifft(...)) pattern, which causes some issues with conjugate spectra :/
          s.sz=size(s.fids);
          s.dims.subSpecs=0;
          if s.dims.averages>0
            s.averages=s.sz(s.dims.averages);
          else
            s.averages=1;
          end;
          s.rawAverages=s.averages;
          ref_cc=s;
        else
          warning('Water reference requested, but not available in this dataset.');
          ref_cc=[];
        end
      end

      if (nargout>=3) % also return preprocessd parts
        % preprocessed data has been requested too; handle subspectra in some sort of way...
        s.averages=1;                    % current number of averages, as it is processed
        s.flags.averaged=1;
        s.flags.leftshifted=1;
        s.flags.filtered=1;
        s.flags.zeropadded=1;
        s.flags.freqcorrected=1;
        s.flags.phasecorrected=1;
        s.flags.subtracted=1;
        ss=0;
        tk={'target','target2'};
        for vv=1:1+any(MRS_struct.p.PRIAM)
          for tt=1:1+any(MRS_struct.p.HERMES)
            s.fids(:,ss+1)=conj(MRS_struct.spec.(MRS_struct.p.Vox{vv}).(tk{tt}).diff);
            s.fids(:,ss+2)=conj(MRS_struct.spec.(MRS_struct.p.Vox{vv}).(tk{tt}).on);
            s.fids(:,ss+3)=conj(MRS_struct.spec.(MRS_struct.p.Vox{vv}).(tk{tt}).off);
            ss=ss+3;
          end
        end
        preprocessed=ss;
      end
    end

    function MRS_struct=to_MRS_struct(met,ref,MRS_struct_in)
      % to_MRS_struct converts data in FID-A format to a Gannet-style MRS_struct
      dbg=0;
      if nargin<2
        ref=[]
      end

      if nargin>=3
        MRS_struct=MRS_struct_in;
      else
        if exist('GannetPreInitialise')
          MRS_struct=GannetPreInitialise();
          if dbg
            disp('Pre-initialised values:')
            MRS_struct
            MRS_struct.p
          end
        else
          MRS_struct.seqorig='??';
          MRS_struct.p.target='GABAGlx';
          MRS_struct.p.target2='Glx';
          MRS_struct.p.ONOFForder='offfirst';
          MRS_struct.p.Water_Positive=1;
          MRS_struct.p.water_phase_correction=0;
          MRS_struct.p.data_phase_correction=0;
          MRS_struct.p.water_removal=0;
          MRS_struct.p.AlignTo='SpecReg';
          MRS_struct.p.Vox={'vox1','vox2'};
          MRS_struct.p.GSH_model='FiveGauss';
          MRS_struct.p.HERMES=0;
          MRS_struct.p.PRIAM=0;
          MRS_struct.p.phantom=0;
          MRS_struct.p.mat=0;
          MRS_struct.p.sdat=0;
          MRS_struct.p.csv=0;
        end

        MRS_struct.version.load='MRS_interop_FIDA';
        MRS_struct.ii=1;
        MRS_struct.p.vendor='??';
        MRS_struct.p.LB=0;
        MRS_struct.p.zf=1;
        MRS_struct.p.Siemens_type=1;
        MRS_struct.p.global_rescale=1;

        % populate with values from met

        MRS_struct.p.LarmorFreq=met.txfrq/1e6;
        MRS_struct.p.TR=met.tr;
        MRS_struct.p.TE=met.te;
        MRS_struct.p.nrows=met.averages;
        MRS_struct.p.Navg=met.rawAverages;
        MRS_struct.p.npoints=met.sz(met.dims.t);
        MRS_struct.p.sw=met.spectralwidth;
      end

      if isstruct(ref)
        MRS_struct.p.ReferenceCompound='H2O';
        MRS_struct.p.Nwateravg=ref.averages;
      end

      MRS_struct.spec.freq=met.ppm;

      % convert FID-A structure into Gannet-compatible MRS_struct
    end

    function res=check_support(silent)
      if (nargin==0) silent=0; end;
      res=struct('read',0,'write',0,'import',0,'run',0,'intermediate',1);
      % check dependencies

      if ~exist('io_loadspec_GE')
        if (~silent) warning('FID-A does not appear to be available.'); end;
        res.intermediate=0;
      end;
    end

    function res=can_import(d)
      % check whether this module is able to import data in the supplied format
      res=0;
      if ischar(d) && exist(d,'file')
        [in_base,in_name,in_ext]=fileparts(d);
        if strcmp(in_ext,'.7')
          if exist('io_loadspec_GE')
            res=1;
          else
            warning('io_loadspec_GE is not available. Please check that FID-A is installed and included in your Matlab search path.');
          end
        elseif strcmpi(in_ext,'.dat') % siemens?
          if exist('io_loadspec_twix')
            res=1;
          else
            warning('io_loadspec_twix is not available. Please check that FID-A is installed and included in your Matlab search path.');
          end
        elseif strcmpi(in_ext,'.sdat') % philips
          if exist('io_loadspec_sdat')
            res=1;
          else
            warning('io_loadspec_sdat is not available. Please check that FID-A is installed and included in your Matlab search path.');
          end
        end
      end
    end
  end

end
