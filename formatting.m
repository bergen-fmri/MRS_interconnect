classdef formatting < handle
  % formatting : a collection of output-formatting functions.
  %
  % Alexander R. Craven, University of Bergen, 2018-10-17

  methods(Static)
    function str=format_exception(ex)
      % FORMAT_EXCEPTION returns a single-line string summarising details of an exception

      stack_elements={};
      for i=1:min(3,length(ex.stack))
        si=ex.stack(i);
        stack_elements{end+1}=sprintf('%s (%s)',si.name,formatting.editor_link(si.file,si.line));
      end
      stack_line=sprintf('%s <= ', stack_elements{:});
      stack_line=stack_line(1:end-4); % remove trailing <=
      str=sprintf('%s : %s [%s]', ex.identifier, ex.message, stack_line);
    end

    function str=editor_link(file, line, text)
      % EDITOR_LINK returns a string incorporating a link to the matlab internal editor...
      %
      % EDITOR_LINK(FILE, LINE, TEXT) returns a string containing a link to open the matlab internal editor with file FILE, positioning cursor at line LINE, with text given by TEXT
      % EDITOR_LINK(FILE, LINE) as above, with text specified by the file's basename and line number, ie basename(FILE):LINE

      if (nargin<3)
        [fb,fn,fx]=fileparts(file);
        text=sprintf('%s:%d',fn,line);
      end

      str=sprintf('<a href="matlab: opentoline(''%s'',%d,0)" style="font-weight:bold;">%s</a>',file,line,text);
    end

    function str=tostring(val, ll, recursion_limit) % {{{
      % TOSTRING(VAL,LL) returns a short string representation of VAL, limited to approximately LL characters (if specified)

      if nargin<2
        ll=100;
      end

      if nargin<3
        recursion_limit=2;
      end

      if iscell(val) && ~any(size(val)>1)
        val=val{1};
      end

      if ischar(val)
        str=val;
      else
        if any(size(val)>1)
          szstr=sprintf('%dx',size(val));
          szstr=szstr(1:end-1);
          str=['(' szstr ')'];
        else
          szstr='';
          str='';
        end

        if isnumeric(val)
          if any(size(val)>1)
            str=['(' szstr '):[' num2str(val(:)') ']'];
          else
            str=num2str(val(:)');
          end
        else
          str=[str ' ' class(val)];
          fields=[];
          if isstruct(val)
            fields=fieldnames(val);
          elseif isa(val,'containers.Map')
            fields=keys(val);
          end
          if ~isempty(fields)
            str=[ str ' : {'];
            for j=1:length(fields)
              if recursion_limit>=0
                if isstruct(val)
                  str=[str ' ' fields{j} '=' formatting.tostring(val(1).(fields{j}),ll,recursion_limit-1)];
                else
                  str=[str ' ' fields{j} '=' formatting.tostring(val(fields{j}),ll,recursion_limit-1)];
                end
              else
                str=[str ' ' fields{j} ];
              end
            end
            str=[ str '}' ];
          end

          %  disp(sprintf('  %20s.%-10s : %-50s',kk,fields{j},vvv(1:min(50,end))));
        end

      end % if ischar(val)

      if length(str)>ll
        str=[str(1:ll-2) '..'];
      end
    end % tostring }}}
      
  end %static methods
end % classdef
