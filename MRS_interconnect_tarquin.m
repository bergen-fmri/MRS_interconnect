classdef MRS_interconnect_tarquin < MRS_interconnect_LCModel
  % MRS_interconnect_tarquin
  %
  % This module supplies data to tarquin in LCModel RAW format; the bulk of the work is therefore handled by the MRS_interconnect_LCModel module.
  %
  % Alexander R. Craven, University of Bergen, 2018-10-17

  properties (Access=private)
  end

  methods % {{{

    function self=MRS_interconnect_tarquin(d,varargin)
      self=self@MRS_interconnect_LCModel(d,varargin{:});
      %self.import(d)
    end

    function res=write(self)
      % WRITE Export data in LCModel RAW format (Tarquin can read this)
      write@MRS_interconnect_LCModel(self);
    end

    function res=run(self,varargin)
      % RUN calls external tarquin binary, with previously exported data
      self.log_provenance('Quant');

      variations=self.get_subspec_variations(varargin{:});
      for vv=1:length(variations)
        % call (external) program to perform analysis on previously-written data
        %cfn=fullfile(self.output_base,'lcm',self.basename);
        filename_variant=self.get_filename_variant(variations{vv}{:});
        fn_txt=self.get_filename([ filename_variant '.tarquin.txt' ]);
        fn_csv=self.get_filename([ filename_variant '.tarquin.csv' ]);
        fn_pdf=self.get_filename([ filename_variant '.tarquin.pdf' ]);
        fn_ref=self.get_filename([ filename_variant '.REF' ]);
        fn_raw=self.get_filename([ filename_variant '.RAW' ]);

        [vox,target,spec]=self.decode_selector_args(variations{vv}{:});

        if strcmp(spec,'diff')
          int_basis='megapress_gaba';
        else
          int_basis='1h_brain';
        end

        [folder,n,e]=fileparts(fn_csv);
        
        % TODO parameters need to adjust according to sequence
        % FIXME tarquin path should be configurable

        tarquin_cmd=sprintf('cd %s; tarquin --format lcm --input %s --input_w %s --auto_phase false --auto_ref false --int_basis %s --output_txt %s --output_csv %s --output_pdf %s --echo 0.068 --fs 5000 --ft 127715123 --w_att 0.76 --w_conc 35880 2>&1',folder,fn_raw,fn_ref,int_basis,fn_txt,fn_csv,fn_pdf);
        self.log(['Running Tarquin: ' tarquin_cmd]);
        [stat,res]=system(tarquin_cmd);

        if (stat~=0)
          warning('Tarquin returned a non-zero error code. This is not a good sign.');
          self.log(sprintf('Tarquin returned error code %d',stat));
        else
          self.log('Tarquin returned with success.');
        end
      end
    end

    function all_res=read(self, varargin)
      % READ import results from external analysis

      % initialise storage tables
      all_res=datatable_ARC();

      variations=self.get_subspec_variations(varargin{:});

      for vv=1:length(variations)


        [vox,target,spec]=self.decode_selector_args(variations{vv}{:});

        res=ordered_map_ARC({'label','vox','target','spec','filename','provenance'},{self.label,vox,target,spec,self.get_filename(),self.get_provenance_string()});

        filename_variant=self.get_filename_variant(variations{vv}{:});
        fn_txt=self.get_filename([ filename_variant '.tarquin.txt' ]);
        self.log(['Tarquin: Reading results from ' fn_txt]);

        if (~exist(fn_txt,'file')) 
          self.log('Read Tarquin TXT output failed! File does not exist. Did Tarquin run correctly?','error');
          continue;
        end;

        f=fopen(fn_txt,'r');
        mode=0;
        skip=0;
        while ~feof(f)
          line=strtrim(fgets(f));
          if (skip>0)
            skip=skip-1;
            continue;
          end

          key=[];
          val=[];

          if (mode==0)
            x=regexp(line,'^Signal.*Conc.*SD.*$');
            if ~isempty(x)
              skip=1;
              mode=1;
            end
          elseif (mode==1)
            if (strcmp(line,'Fit quality'))
              mode=2;
              skip=1;
            end;
          elseif (mode==2)
            if strcmp(line,'Fitting parameters')
              mode=3;
            end
          elseif (mode==3)
            % do nothing for fitting parameters section yet.
            if strcmp(line,'Concentration scaling')
              mode=4;
            end
          end

          if (mode==2 || mode==4)
            bits=regexp(line,'^(?<key>[a-zA-Z0-9() ]+)[ \t]*:[ \t]*(?<val>[-a-zA-Z0-9.e+]+)','names');
            if ~isempty(bits)
              key=strtrim(bits(1).key);
              val=strtrim(bits(1).val);
              dmatch=regexp(val,'^[-+.0-9eE]+$');
              if dmatch
                res(key)=str2num(val);
              else
                res(key)=val;
              end
            end
          elseif (mode==1)
            bits=regexp(line,'^(?<metab>[a-zA-Z0-9_]+)[ \t]*(?<conc>[-+.0-9eE]+)[ \t]*(?<sd>[-+.0-9eE]+)[ \t]*(?<sd_mm>[-+.0-9eE]+)$','names');
            if ~isempty(bits)
              key1=strtrim(bits(1).metab);
              val1=str2num(strtrim(bits(1).conc));
              key2=[key1 ' %SD'];
              val2=str2num(strtrim(bits(1).sd));
              res(key1)=val1;
              res(key2)=val2;
            end
          end
        end
        fclose(f);

        % force metabs
        keys={'Glx_A','Glx_B','Glx_C','Glx_D','NAA','TGABA',          'Glx_A %SD','Glx_B %SD','Glx_C %SD','Glx_D %SD','NAA %SD','TGABA %SD'};
        for ki=1:length(keys)
          k=keys{ki};
          if ~res.isKey(k)
            res(k)=NaN;
          end;
        end;
        all_res.append(res);
      end
    end

    function res=wrap(self)
      % WRAP export data, run the analysis and return the results
      
      % Although we inherit from LCModel, skip that wrapper and use the standard base function
      res=wrap@MRS_interconnect_base(self);
    end

  end % }}}

  methods(Static)

    function res=check_support(silent)
      % CHECK_SUPPORT reports capabilities of this module, in the current configuration.
      %
      % See Also: MRS_interconnect_base.check_support

      res=struct('read',1,'write',1,'import',0,'run',0,'intermediate',0);
      if (nargin==0) silent=0; end;

      if isunix || ismac
        [stat,txt]=system(['which tarquin']);
        if stat~=0
          if (~silent) warning('Tarquin could not be found.'); end;
          
        else
          res.run=1;
        end;
      end;
      
    end

  end

end
