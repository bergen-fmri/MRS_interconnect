classdef datatable_ARC < ordered_map_ARC
  % datatable_ARC   stores multiple rows of keyed data, in an orderly structure
  %   The table may be subsequently queried on numeric or column-name keys.
  %
  % See also ordered_map_ARC 
  %
  % This implementation is EXPERIMENTAL (and, in some aspects, known to be
  % suboptimal); use with caution.
  %
  % Alex Craven, University of Bergen

  properties (Access=protected)
    nrows;
  end

  methods % {{{

    function obj=datatable_ARC(a,b)
      obj=obj@ordered_map_ARC();
      obj.nrows=0;
      if nargin==2
        obj.append(a,b);
      end
    end

    function obj=grow(obj,rown) % {{{
      if rown>obj.nrows
        obj.nrows=rown; % must update this before doing subsequent subsasgn call, to avoid recursion....
        keys=obj.keys();
        for i=1:length(keys)
          s=struct();
          s.type='()';
          s.subs={keys{i}};
          v=obj.subsref(s);
          if (length(v)<rown)
            if iscell(v)
              v{rown,1}=[];
            else
              v(rown,1)=NaN;
            end
          end;
          sref=obj.subsasgn(s,v);
        end;
      end;
    end; % function grow }}}

    function sz=size(obj, varargin)
      if length(varargin)>1
        sz=builtin('size',obj,varargin{:});
      else
        sz=[obj.nrows,length(obj.order)];
        if length(varargin)>0
          sz=sz(varargin{1});
        end
      end
    end
      
    function varargout=subsref(obj,s) % {{{
      % SUBSREF
      % one numeric argument: return row
      % numeric argument followed by something else (numeric or not...) : field ref in specific row
      % otherwise : default ordered_map_ARC behaviour (this is probably going to return a full column)
      if (strcmp(s(1).type,'()') && length(s(1).subs)<3 && length(s(1).subs)>0 && isnumeric(s(1).subs{1}))
        row=obj.get_row(s(1).subs{1});
        if length(s(1).subs)==2
          [varargout{1:nargout}]=row(s(1).subs{2});
        else
          [varargout{1:nargout}]=row;
        end
      else
        [varargout{1:nargout}] = subsref@ordered_map_ARC(obj,s);
      end
    end;

    % }}}

    function sref=subsasgn(obj,S,B) % {{{
      switch S(1).type
        % ref 1: http://se.mathworks.com/help/matlab/matlab_oop/class-with-modified-indexing.html
        % ref 2: http://se.mathworks.com/help/matlab/matlab_oop/overloading-numel-subsref-and-subsasgn.html
        case '()'
          if (length(S.subs)==2)
            key=S.subs{1};
            row=S.subs{2};
            ns=S;
            ns.subs={key};
            obj.grow(row); % make sure there's space for this row.
            if isempty(B) % adding an empty row... the grow operation above is enough
              sref=obj;
              return
            end
            if ~obj.isKey(key)
              col=NaN*zeros(row,1);
            else
              col=obj.subsref(ns);
            end
            if ~isnumeric(B) && ~iscell(col)
              ccol=num2cell(col);  % not {col}. which creates a 1x1 cell containing the nx1 values
              for i=find(reshape(isnan(col),1,[])) % make the NaNs empty
                ccol{i}=[];
              end
              col=ccol;
            end
            if iscell(col)
              %if ~ischar(B) && length(B)>1
              %  warning('trying to assign multiple values? this might cause trouble');
              %end
              col{row,1}=B;
            else
              col(row,1)=B;
            end
            sref=subsasgn@ordered_map_ARC(obj,ns,col);
          elseif ((length(S.subs)==1) && length(B)==obj.nrows) % storing a column vector?
            sref=subsasgn@ordered_map_ARC(obj,S,B);
          else
            error('datatable assignment should be key-value pair; use append to add an entire row')
          end
        case '.'
          sref=builtin('subsref',obj,s);
          % check nargout?  if nargout == 0 builtin('subsref', this, S); ...
        case '{}'
          sref=subsasgn@containers.Map(obj,S,B);
      end
    end % function subsasgn }}}

    function obj=append(obj,a,b) % {{{
      new_rows=0;
      multi=0;
      cell_rowfirst=0;
      cell_colfirst=0;
      if (nargin==3) % append key/value pairs (single row, or multiple rows in value matrix)
        keys=a;
        vals=b;
        if iscell(vals)
          if any(size(vals)==0) % nothing to add...
            return
          end
          if all(size(keys)==size(vals))
            multi=0;
            new_rows=1;
            row_of_col_vecs=0;
            % HOWEVER, we also have to deal with the case of a cell array of column vectors:
            has_non_empty=0;
            mask_cells  =cellfun(@(x) iscell(x),    vals);
            mask_numeric=cellfun(@(x) isnumeric(x), vals);
            mask_strings=cellfun(@(x) ischar(x),    vals);

            vect_length =zeros(1,length(vals));
            vect_length(mask_cells|mask_numeric)=cellfun(@(x) length(x), vals(mask_cells|mask_numeric));
            vect_length(mask_strings)=1;

            mask_empty  =cellfun(@(x) isempty(x), vals);
            mask_empty(mask_cells&(vect_length==1))=cellfun(@(x) isempty(x{1}), vals(mask_cells&(vect_length==1)));

            vectormask=cellfun(@(x) iscell(x),vals);
            vectormask=vectormask | (cellfun(@(x) isnumeric(x) && length(x)>1, vals));

            if all(mask_empty)
              new_rows=0;
            else
              new_rows=max(vect_length(:));
            end

            if sum(mask_cells(:))>0
              row_of_col_vecs=1;
            end;

            if new_rows>1 || row_of_col_vecs
              cell_colfirst=1;
              multi=1;
            end
          else
            multi=1;
            new_rows=length(vals);
          end
          cell_rowfirst=~cell_colfirst;
        else
          new_rows=1;
        end
      elseif isempty(a) % nothing to add.
        return;
      elseif isa(a,'ordered_map_ARC') % append another ordered map (can be multi-row datatable or single-row ordered_map_ARC)
        keys=a.order;
        vals={};
        for i=1:length(keys)
          vals{i}=1;
          s=struct();
          s.type='()';
          s.subs={keys{i}};
          vals{i}=a.subsref(s);
          if (isnumeric(vals{i})||iscell(vals{i})) % string values (with length>1) confused this previously
            new_rows=max(length(vals{i}),new_rows);
          else
            new_rows=max(new_rows,1);
          end;
        end
        if new_rows>1
          multi=1;
        end
      elseif isa(a,'containers.Map') % append an unordered map
        keys=a.keys;
        vals=a.values;
        new_rows=1;
      elseif isstruct(a)
        keys=fields(a);
        vals={};
        for i=1:length(keys)
          vals{i}=a.(keys{i});
        end
      else
        error('Unsupported data type for datatable.append()');
      end
      if multi % add multiple rows
        nrows_pre=obj.nrows; % note, assignment will change obj.nrows
        for row_in=1:new_rows
          row_out=nrows_pre+row_in;
          if cell_rowfirst
            rr=vals{row_in};
          end
          for i=1:length(keys)
            s=struct(); s.type='()'; s.subs={keys{i},row_out};
            if cell_rowfirst
              vvv=rr(i);
            else
              vv=vals{i};
              if iscell(vv)
                vvv=vv{row_in};
              else
                vvv=vv(row_in);
              end
              %vvv=vv(row_in);
            end
            obj=obj.subsasgn(s,vvv);
          end
        end
      else % add single row (also handles the case of column-wise cell input)
        row_out=obj.nrows+1;
        if ~all(cellfun(@isempty,vals))
          for i=1:length(keys)
            s=struct();
            s(1).type='()';
            s.subs={keys{i},row_out};
            obj=obj.subsasgn(s,vals{i});
            %obj(keys{i},row_out)=vals{i};
          end
        end
      end
    end % append }}}

    function obj=vertcat(varargin) % {{{
      obj=varargin{1};
      for x=2:length(varargin)
        obj.append(varargin{x});
      end
    end % }}}

    function col=get_col(obj,k)
      s=struct(); s.type='()'; s.subs={k};
      col=obj.subsref(s);
    end;

    function col=unique(obj,k)
      allvals=obj.get_col(k);
      % can use built-in unique for uniform cases...
      if all(isnumeric(allvals))
        col=unique([allvals{:}]);
      elseif all(ischar(allvals))
        col=unique(allvals);
      else % otherwise, mixed, use a hash container to unique-ify
        m=containers.Map();
        for i=1:length(allvals)
          k=strtrim(char(allvals{i}));
          if ~isempty(k) && size(k,1)==1
            m(char(allvals{i}))=allvals{i};
          end
        end
        col=m.keys();
      end
    end

    function dt=filter(obj,varargin) % {{{
      if (nargin<3||(mod(nargin,2)==0))
        error('datatable filter requires one or more PAIRS of key and condition');
      end

      [operator,args]=get_kwarg('operator','AND',varargin{:});
      [match,args]=get_kwarg('match','full',args{:});
      [missing,args]=get_kwarg('missing','fail',args{:});

      ni=obj.nrows;
      mask=[]; % we'll put test outcome for each key/cond into a new column of mask
      for i=1:2:length(args)
        key=args{i};
        cond=args{i+1};
        switch lower(missing)
          case 'true'
            if ~any(strcmpi(obj.keys(),key))
              mask(1:ni,end+1)=1;
              continue;
            end;
          case 'false'
            if ~any(strcmpi(obj.keys(),key))
              mask(1:ni,end+1)=0;
              continue;
            end
        end
        col=obj.get_col(key);
        if isnumeric(col)
          row_mask=col==cond;
        elseif isnumeric(cond)
          row_indices=[];
          warning('incompatible data types; no rows will be returned.')
        else
          switch lower(match)
            case 'substring'
              row_mask=cellfun(@(y) ~isempty(strfind(lower(y),lower(cond))),col);
            otherwise
              row_mask=strcmpi(cond,col);
          end
        end
        mask(1:ni,end+1)=row_mask;
      end

      switch lower(operator)
        case 'and' % all rows must match
          row_indices=find(min(mask,[],2));
        case 'or' % any row matches
          row_indices=find(max(mask,[],2));
        otherwise
          error(['Invalid operator specified: ' operator])
      end

      order=obj.order;
      keys=obj.keys();
      vals=obj.values();
      ko={};
      vo={};
      for ordered=1:length(order)
        i=min(find(strcmp(order{ordered},keys)));
        vv=vals{i};
        vvv=vv(row_indices);
        ko{end+1}=keys{i};
        vo{end+1}=vvv;
      end
      dt=datatable_ARC(ko,vo);
    end % }}}

    function row=get_row(obj,a,b)
      if nargin<3 && isnumeric(a)
        order=obj.order;
        keys=obj.keys();
        vals=obj.values();
        ko={};
        vo={};
        for ordered=1:length(obj.order)
          ki=min(find(strcmp(order{ordered},keys)));
          vv=vals{ki};
          if iscell(vv)
            vv=vv{a,1};
          else
            vv=vv(a,1);
          end
          if isscalar(vv)||ischar(vv)
            if ~isempty(vv) && (ischar(vv)||~isnan(vv))
              ko{end+1}=keys{ki};
              vo{end+1}=vv;
            end;
          else
            %vv
            %warning('odd value')
          end
        end;
        row=[];
        row=ordered_map_ARC(ko,vo);
        %row.show()
      else
        col=obj.get_col(a);
        row_index=find(strcmpi(b,col));
        if ~isempty(row_index)
          row=obj.get_row(min(row_index));
        end
      end
    end

    function obj=show(obj) % {{{
      % SHOW displays a summary of the contents of this table on the console

      if obj.nrows==0
        disp(sprintf('datatable is empty (0 rows, %d columns)',length(obj.order)));
        return
      else
        disp(sprintf('datatable contains %d rows, %d columns', obj.nrows, length(obj.order)));
      end

      cw=9;
      cwv=cw;

      transpose=(obj.nrows==1 && length(obj.order)>8); % special case for wide, one-row tables: display transposed;

      if transpose
        cw=30;
        cwv=99;
      end

      % display column headers...
      cols={};
      for i=1:length(obj.order)
        k=obj.order{i};
        if (length(k)>cw) ks=k(1:cw); else ks=k; end;
        cols{end+1}=ks;
      end

      if ~transpose
        % show column header row
        disp([',' repmat('-----------.',1,length(cols))]);
        disp(['|' sprintf(' %9s |',cols{:})])
        disp(['+' repmat('-----------+',1,length(cols))]);
      end


      % now, row-by-row display of values. this is not particularly efficient :(

      for i=1:obj.nrows
        cols={};
        for j=1:length(obj.order)
          col=obj.get_col(obj.order{j});
          if length(col)~=obj.nrows
            warning('col size mismatch!')
          end
          if iscell(col)
            val=col{i};
          else
            val=col(i);
          end
          if isempty(val)
            ks='.';
          elseif isscalar(val)&&isnumeric(val)
            if isinteger(val)||(floor(val)==val) % turns out isinteger isn't that reliable at actually determining whether something is an integer.
              ks=sprintf('%d',val);
            else
              oom=log10(val);
              if (oom<-1 || oom>5)
                ks=sprintf('%8.2e',val);
              else
                ks=sprintf('%8.2f',val);
              end
            end
          elseif iscell(val) && all(isempty(val{:}))
            ks='.';
          elseif isstr(val)||iscell(val)
            ks=char(val);
          elseif length(val)>0
            ks=sprintf('[%dx%d]',size(val,1),size(val,2));
          else
            ks='?!';
          end
          if (length(ks)>cwv) ks=ks(1:cwv); end;
          cols{end+1}=ks;
        end;
        if ~transpose
          disp([ '|' sprintf(' %9s |',cols{:})]);
        end
      end;

      if transpose
        %rows=cols;
        for j=1:length(obj.order)
          k=obj.order{j};
          if (length(k)>cw) ks=k(1:cw); else ks=k; end;
          disp(sprintf(' %30s : %s',ks,strtrim(cols{j})))
          %cols={ks};
          %cols{end+1}=rows{j};
          %disp([ '|' sprintf(' %20s |',cols{:})]);
        end
      else
        disp(['`' repmat('-----------''',1,length(cols))]);
      end
    end % show }}}

    function obj=save(obj,fn) % {{{
      % SAVE export this table in CSV format

      fid=fopen(fn,'wt');

      cols={};
      head_str='';
      for i=1:length(obj.order)
        k=obj.order{i};
        k=strrep(k,',',' '); % filter commas which will confuse col order in un-quoted csv output
        cols{end+1}=k;
        if i==1
          head_str=k;
        else
          head_str=[head_str ',' k];
        end
      end

      %disp(head_str);
      fwrite(fid,sprintf('%s\n',head_str));

      % now, row-by-row output of values. this is not particularly efficient :(

      for i=1:obj.nrows
        row_str='';
        cols={};
        for j=1:length(obj.order)
          col=obj.get_col(obj.order{j});
          if length(col)~=obj.nrows
            warning('col size mismatch!')
          end
          if iscell(col)
            val=col{i};
          else
            val=col(i);
          end
          if isempty(val)
            ks='';
          elseif isscalar(val)&&isnumeric(val)
            if isinteger(val)||(floor(val)==val) % turns out isinteger isn't that reliable at actually determining whether something is an integer.
              ks=sprintf('%8d',val);
            else
              oom=log10(val);
              if (oom<-1 || oom>5)
                ks=sprintf('%8.2e',val);
              else
                ks=sprintf('%8.2f',val);
              end
            end
          elseif iscell(val) && all(isempty(val{:}))
            ks='';
          elseif isstr(val)||iscell(val)
            ks=char(val);
          elseif length(val)>0
            ks=sprintf('[%dx%d]',size(val,1),size(val,2));
          else
            ks='?!';
          end
          ks=strrep(ks,',',' '); % filter commas which will confuse col order in un-quoted csv output
          cols{end+1}=ks;
          if j==1
            row_str=ks;
          else
            row_str=[row_str ',' ks];
          end
        end % j : cols
        fwrite(fid,sprintf('%s\n',row_str));
      end % i : rows
      fclose(fid);

    end % show }}}
      
  end % methods }}}

end
