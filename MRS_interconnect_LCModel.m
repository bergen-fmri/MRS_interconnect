classdef MRS_interconnect_LCModel < MRS_interconnect_base
  % MRS_interconnect_LCModel : LCModel support module
  %
  % This module provides export capabilities (RAW and control), import capabilities (table, coords,
  % csv, basis), and basis set selection.
  %
  % Alexander R. Craven, University of Bergen, 2018-10-17

  properties (Access=private)
    vendor_ge=[];
    control=[];
    table=[];
    csv=[];
    coords=[];
  end

  methods % {{{

    function self=MRS_interconnect_LCModel(d,varargin)
      self=self@MRS_interconnect_base(d,varargin{:});
    end

    function res=write(self,varargin)
      % WRITE exports RAW data and corresponding .control files, in preparation for analysis with
      % lcmodel.
      %
      % Handles multiple edit targets (HERMES), multiple voxels (PRIAM), separate ON/OFF/DIFF parts
      %
      % Does not handle: multiple iterations (ii)

      [variations,extra_args]=self.get_subspec_variations(varargin{:});

      for vv=1:length(variations)
        [vox,target,spec]=self.decode_selector_args(variations{vv}{:});

        prefix='';

        if isfield(self.MRS_struct.spec.(vox),'water')
          ref= ifft(ifftshift(self.MRS_struct.spec.(vox).water));         %, self.MRS_struct.p.npoints);
        else
          ref=[];
        end

        if isempty(ref)
          self.log('Quantifying without water reference!');
        else
          % Gannet's water refs come through as complex conjugate; detect and correct
          if sum(abs(ref(1:end/2)))<sum(abs(ref(end/2:end)))
            warning('Input water spectrum is complex conjugate... correcting');
            ref=ifft(ifftshift(conj(self.MRS_struct.spec.(vox).water)));
            self.log('Corrected complex-conjugate input spectrum (reference)');
          end
        end

        diff=self.MRS_struct.spec.(vox).(target).(spec);

        if strcmp(spec,'diff')
          % Check for correct ON/OFF order (check for strong negative NAA in diff)
          neg_mask=(self.MRS_struct.spec.freq>1.7) & (self.MRS_struct.spec.freq<2.1);

          if max(-real(diff(neg_mask)))<max(real(diff(neg_mask)))
            warning('DIFF spectrum appears to be inverted; check ON-OFF order! (correcting)');
            self.log('Corrected seemingly incorrect ON-OFF order');
            diff=-diff;
          end
        else
          % Check for inverted spectrum (check for positive peaks in metab range)
          neg_mask=(self.MRS_struct.spec.freq>2.8) & (self.MRS_struct.spec.freq<4.2);
          if max(-real(diff(neg_mask)))>max(real(diff(neg_mask)))

            if strncmpi('philips',self.MRS_struct.p.vendor,7)&&self.MRS_struct.p.Water_Positive
              warning('Metabolite spectrum appears to be inverted; check Water_Positive config');
            else
              warning('Metabolite spectrum appears to be inverted');
            end
            self.log('Corrected seemingly inverted metabolite spectrum');
            diff=-diff;
          end
        end

        data=ifft(ifftshift(diff));

        % FID-A preprocessor gives us complex conjugate data; detect and correct
        if sum(abs(data(1:end/2)))<sum(abs(data(end/2:end)))
          warning('Input metabolite spectrum is complex conjugate... correcting');
          self.log('Corrected complex-conjugate input spectrum (metabolite)');
          diff=conj(diff);
          data=ifft(ifftshift(diff));
        end

        plot_range=(self.MRS_struct.spec.freq>0) & (self.MRS_struct.spec.freq<5.0);

        MRS_plot.figure();

        npts=self.MRS_struct.p.npoints;

        %-----
        subplot(2,2,1);
        plot(self.MRS_struct.spec.freq(plot_range),real(diff(plot_range)),'DisplayName','diff');
        title([prefix ' MET'])
        MRS_plot.format_freq();
        MRS_plot.gaba_markers();
        MRS_plot.metab_scale();

        %-----
        subplot(2,2,2);
        plot(real(data(1:min(end,npts))));
        MRS_plot.format_time();
        title([prefix ' MET (time domain)'])

        if ~isempty(ref)
          %-----
          subplot(2,2,3);
          plot(self.MRS_struct.spec.freq(plot_range),real(diff(plot_range)),'Color',[.8,.8,.9],'DisplayName','diff');
          hold on;
          plot(self.MRS_struct.spec.freq(plot_range),real(self.MRS_struct.spec.(vox).water(plot_range)),'DisplayName','REF');
          title([prefix ' ref'])
          MRS_plot.format_freq();
          MRS_plot.gaba_markers()

          %-----
          subplot(2,2,4);
          plot(real(ref(1:min(end,npts))),'k','DisplayName','REF');
          MRS_plot.format_time();

          if strcmp(class(ref),'single')
            % check data type of input. sometimes (Siemens import via FID-A) it comes through as "single". The only thing this actually breaks so far is the text() call below.
            self.log('Reference is of type "single" (rather than the more usual "double"); this tends to happen with Siemens data loaded via FID-A. Casting to double...');
            ref=double(ref);
          end

          text(length(ref),0.3*min(real(ref)),sprintf('nunfil %d; deltat %.1fms (T=%.1f ms)', self.MRS_struct.p.npoints, 1e3/self.MRS_struct.p.sw, 1e3*self.MRS_struct.p.npoints/self.MRS_struct.p.sw),'HorizontalAlignment','right','Color',[.5,0,0]);
          title([prefix ' ref (time-domain)']);
          MRS_plot.format_time();
        end

        MRS_plot.suptitle(sprintf('LCModel export: %s %s %s', vox, target, spec),self.get_provenance_string());
        MRS_plot.export(self.get_filename([self.get_filename_variant(variations{vv}{:}) '_01_export.png']))

        drawnow();

        self.write_LCModel_RAW(data,ref,variations{vv}{:},extra_args{:})

      end

    end

    function res=run(self,varargin)
      % RUN invokes LCModel on each of the control files, previously generated by write()

      self.log_provenance('Quant');

      variations=self.get_subspec_variations(varargin{:});
      for vv=1:length(variations)
        % call (external) program to perform analysis on previously-written data
        %cfn=fullfile(self.output_base,'lcm',self.basename);
        cfn=self.get_filename([self.get_filename_variant(variations{vv}{:}) '.control']);
        lcmodel_command=sprintf('%s 2>&1 < %s',MRS_interconnect_config.get('command_lcmodel'), cfn);
        self.log(sprintf('Running: %s (WD: %s)',lcmodel_command,pwd))
        [stat,res]=system(lcmodel_command);
        if (stat~=0)
          warning('LCModel returned a non-zero error code. This is not a good sign.');
          self.log(sprintf('LCModel returned error code %d',stat));
        else
          self.log('LCModel returned with success (this doesn''t necessarily mean that it actually worked...).');
        end
      end
    end

    function res=read(self,varargin)
      % READ reads results from a previous run()

      % initialise storage tables
      res=datatable_ARC();

      variations=self.get_subspec_variations(varargin{:});

      for vv=1:length(variations)

        % import results from external analysis {{{

        [vox,target,spec]=self.decode_selector_args(variations{vv}{:});
        [site,subj,seq]=self.decode_session_info();
        vv_info=ordered_map_ARC({'label','site','subject','sequence','vox','target','spec','filename','provenance'},{self.label,site,subj,seq,vox,target,spec,self.get_filename(),self.get_provenance_string()});

        vv_control=self.read_control(variations{vv}{:});
        vv_table=self.read_table(variations{vv}{:});

        if ~isempty(vv_table.diags) && isfield(vv_table.diags,'type') && any(strcmp('FATAL',{vv_table.diags.type}))
          self.log('Now LCModel reports some FATAL errors.');
          vv_csv=[];
          vv_coords=[];
        else
          vv_csv=self.read_csv(variations{vv}{:});
          vv_coords=self.read_coords(variations{vv}{:});

          % merge all the parameters into one data row
          if ~isempty(vv_csv)
            vv_info=[vv_info; vv_csv(1)];
          end
          vv_info('coords')=vv_coords;
        end

        % convert ppm units to Hz (for convenience)...
        hzpppm=vv_control(1,'HZPPPM');
        klist={'fwhm','data_shift','alphaB'};
        for i=1:length(klist)
          k=klist{i};
          kHz=[k '_Hz'];
          if isfield(vv_table.parameters,k)
            vv_table.parameters.(kHz)=vv_table.parameters.(k)*hzpppm;
          end
        end

        vv_info('control')=vv_control;
        vv_info('table')=vv_table;
        vv_info=[ vv_info; ordered_map_ARC(vv_table.parameters) ];
        vv_info.show();

        res.append(vv_info);

        % convert ppm to Hz for diag params
        for di=1:length(vv_table.diags)
          %disp(vv_table.diags(i).message)
          self.log([' --> LCModel diagnostics    : ' vv_table.diags(di).message]);
        end
        

        % display output {{{
        if ~isempty(vv_coords)
          MRS_plot.figure();
          subplot(5,1,1:4)

          hold on;
          plot(vv_coords.ppm,vv_coords.data,'k','DisplayName','Data'); 
          plot(vv_coords.ppm,vv_coords.fit,'r','LineWidth',2,'DisplayName','Fit');
          resid=vv_coords.data-vv_coords.fit;

          if ~isempty(vv_coords.baseline)
            plot(vv_coords.ppm,vv_coords.baseline,'b-','DisplayName','Background');
            resid=resid-vv_coords.baseline;
          end

          MRS_plot.format_freq();
          MRS_plot.gaba_markers()

          subplot(5,1,5);
          plot(vv_coords.ppm,resid,'k','LineWidth',1,'DisplayName','Residual');
          MRS_plot.format_freq();
          MRS_plot.gaba_markers()

          MRS_plot.suptitle([ 'LCModel fit output: ' self.get_subspec_string(variations{vv}{:})],self.get_provenance_string());

          MRS_plot.export(self.get_filename([self.get_filename_variant(variations{vv}{:}) '_02_fit.png']))

          drawnow();
        end
        % }}}

        %res=[ self.csv(1); ordered_map_ARC(vv_table.parameters) ]; % take first data row from CSV file
        % }}}
      end % variations

    end

    function csvdat=read_csv(self, varargin) % {{{
      % READ_CSV reads LCModel CSV-format data (currently: row=1, col=1 only) for the specified
      % subspectrum

      fn=self.get_filename([ self.get_filename_variant(varargin{:}) '.csv' ]);

      self.log(['Read LCModel CSV output     : ' fn]);

      if (~exist(fn,'file')) 
        self.log('Read LCModel CSV output failed! File does not exist. Did LCModel run correctly?','error');
        csvdat=[];
        return;
      end;

      allrows=read_csv_ARC(fn);
      csvdat=allrows.filter('Row',1,'Col',1);
      return

    end % read_csv }}}

    function out=read_coords(self,varargin) % {{{
      % READ_COORDS reads LCModel coord data for the specified subspectrum of an instanciated MRS_interconnect_LCModel object
      %
      % See also: MRS_interconnect_LCModel.read_coords_file

      fn=self.get_filename([ self.get_filename_variant(varargin{:}) '.coo' ]);

      self.log(['Read LCModel output coords  : ' fn]);

      if (~exist(fn,'file')) 
        self.log('Read LCModel coords failed! File does not exist. Did LCModel run correctly?','error');
        out=[];
        return;
      end;

      out=MRS_interconnect_LCModel.read_coords_file(fn);

    end % read_coords }}}

    function ctrl=read_control(self,varargin) % {{{
      % READ_CONTROL reads LCModel control parameters for the specified subspectrum

      if length(varargin)==1
        fn=varargin{1};
      else
        fn=self.get_filename([ self.get_filename_variant(varargin{:}) '.control' ]);
      end
      self.log(['Read LCModel control file   : ' fn]);
      if (~exist(fn,'file')) 
        self.log('Read LCModel control file failed! File does not exist.','error');
        ctrl=[];
        return;
      end;
      ctrl=MRS_interconnect_LCModel.read_parameters(fn);
    end % read_control }}}

    function out=read_table(self,varargin) % {{{
      % READ_TABLE reads LCModel table output for the specified subspectrum
      %
      % See also: MRS_interconnect_LCModel_diagnostics

      if length(varargin)==1
        fn=varargin{1};
      else
        fn=self.get_filename([ self.get_filename_variant(varargin{:}) '.table' ]);
      end
      self.log(['Read LCModel output table   : ' fn]);

      out=struct();
      out.parameters.fwhm=-1;
      out.parameters.snr=-1;
      out.diags=[];

      if (~exist(fn,'file')) 
        self.log('Read LCModel output table failed! File does not exist. Did LCModel run correctly?','error');
        return;
      end;
      fh=fopen(fn);
      section='none';
      while (~feof(fh))
        %ln=strtrim(fgets(fh));
        str=fgets(fh);
        if (~isstr(str)) break; end;
        ln=strtrim(str); %fgets(fh));

        if length(ln)==0
          section='none';
          continue;
        end

        if strncmp(ln,'$$MISC',6)
          section='misc';
          continue;
        elseif strncmp(ln,'$$DIAG',6)
          section='diag';
          continue;
        end
        
        switch section
          case 'misc'
            % FWHM = 0.038 ppm    S/N =  23     $
            %x=regexp(ln,'FWHM[ ]*=[ ]*(?<fwhm>[.0-9]+)[ ]+ppm[ ]+S/N[ ]+(?<snr>[.0-9]+)[ ]+$','names');
            x=regexp(ln,'FWHM[\t ]*=[\t ]*(?<fwhm>[.0-9]+)[ \t]*ppm[\t ]*S.N[\t ]*=[\t ]*(?<snr>[.0-9]+)','names');
            if (~isempty(x))
              out.parameters.fwhm=str2double(x.fwhm);
              out.parameters.snr=str2double(x.snr);
              continue;
            end
            x=regexp(ln,'Data shift = (?<shift>[-.0-9]+)[ \t]*ppm','names');
            if ~isempty(x)
              out.parameters.data_shift=str2double(x.shift);
            end
            x=regexp(ln,'Ph:[\t ]*(?<ph0>[0-9]+)[\t ]*deg[\t ]*(?<ph1>[.0-9]+)[\t ]* deg','names');
            if ~isempty(x)
              out.parameters.ph0=str2double(x.ph0);
              out.parameters.ph1=str2double(x.ph1);
            end
            x=regexp(ln,'alphaB,S[\t ]*=[\t ]*(?<alphaB>[-+.E0-9]+)[,\t ]*(?<alphaS>[-+.E0-9]+)','names');
            if ~isempty(x)
              out.parameters.alphaB=str2double(x.alphaB);
              out.parameters.alphaS=str2double(x.alphaS);
            end
            x=regexp(ln,'(?<knots>[0-9]+) spline knots.*Ns[ =]+(?<Ns>[0-9]+)','names');
            if ~isempty(x)
              out.parameters.knots=str2num(x.knots);
              out.parameters.Ns=str2num(x.Ns);
            end
            x=regexp(ln,'(?<inflections>[0-9]+) inflections.*(?<extrema>[0-9]+) extrema','names');
            if ~isempty(x)
              out.parameters.inflections=str2num(x.inflections);
              out.parameters.extrema=str2num(x.extrema);
            end

          case 'diag'
            x=regexp(ln,'(?<num>[.0-9]+)[ \t]*(?<type>[a-zA-Z]+)[\t ]*(?<submodule>[A-Z0-9]+)[\t ]*(?<code>[0-9]+)','names');
            if ~isempty(x)
              x.message=MRS_interconnect_LCModel_diagnostics(x);
              if isempty(out.diags)
                out.diags=x;
              else
                out.diags(end+1)=x;
              end
            end
        end
      end
      fclose(fh);
    end % read_table }}}

    function write_LCModel_RAW(self,data,ref,varargin) %  {{{
      % write_LCModel : Write out data into LCModel RAW format, with corresponding control files.
      %
      % Preferred basis sets should be configured in MRS_interconnect_config.m
      %
      % Non-default CONTROL parameters may be specified as elements of a structure in the 'control'
      % named argument, like:
      %
      %   write_LCModel_RAW(...,'control',{'NRATIO',0,'FILBAS','"/path/to/basisset.basis"'});
      %
      % See Also: MRS_interconnect_LCModel.write, MRS_interconnect_config


      %
      % derived from RTN2015 implementation, with extensions for CSI and multivoxel
      %
      % fname     -- the file name
      % data      -- time-domain (FID) data to write. 
      % header    -- the P-file header info (from [xxxx,header] = read_MR_rawdata(pfile_fname) )
      % par       -- the interpreted P-file parameters (from   par=header2par(header), OR felix's SVInfo structure )
      %
      % data contains FID information, as either:
      %      a single time-series, data(t)=..., 
      %  or, a 2D array of timeseries data: data(row,column,t)=...
      %  or, a set of time rows: data(n,t)=...
      %
      % ref and data should match in size (ie, ref may need to be repeated if data contains multiple subsets)
      %
      % See also:  read_MR_rawdata, header2par

      % For GE format input (with appropriate support functions), we can also write out CSI data....
      %self.check_vendor_extensions();

      if isfield(self.vendor,'GE')
        self.vendor.GE
        self.vendor.GE.MRS_struct
        header=self.vendor.GE.get_header()
        par=self.vendor.GE.get_par()
      else
        header=[];
        par=[];
      end

      if nargin<3
        ref=[];
      end;

      control_override=[];
      control_extra=[];

      vox=1;
      target=1;
      spec='diff';

      while ~isempty(varargin) % {{{
        switch lower(varargin{1})
          case 'target'
            target=varargin{2};
          case 'vox'
            vox=varargin{2};
          case 'control'
            control_override=varargin{2};
          case 'spec'
            spec=varargin{2};
          otherwise
            error(['Unknown option: ' varargin{1}]);
          end % switch
        varargin(1:2)=[];
      end % while }}}

      [vox,target,spec]=self.decode_selector_args('vox',vox,'target',target,'spec',spec);

      self.log(sprintf('write_LCModel_RAW : vox %s, target %s, spec %s', vox, target, spec));

      if 0 && isfield(par,'num_points') && ~isfield(par,'samples')
        % it seems we have a SV_info structure (per Felix's GSH preprocessor). Rearrange the bits we need...
        par.samples=par.num_points;
        if isfield(par,'rdb_hdr')
          par.vox_volume=(par.rdb_hdr.roilenx*par.rdb_hdr.roileny*par.rdb_hdr.roilenz)/1e3;
        end
      end

      if ~isempty(header)
        nc=header.image.dim_X;
        nr=header.image.dim_Y;
        % need to check row/col order! (may be transposed?)
        vol=(header.image.dfov^2*header.image.slthick)/(1000*nr*nc);

        is_csi=(nc>1 || nr>1);
      else
        is_csi=0;
        nr=1;
        nc=1;
      end;

      multifid=0;

      if is_csi
        icolst=1;
        irowst=1;
        icolen=nc;
        irowen=nr;
      else
        icolst=1;
        if ~isempty(par)&&(size(data,1)<par.samples && size(data,2)>=par.samples)
          multifid=1;
          nc=size(data,1);
          icolen=nc;
        else
          icolen=1;
        end
        irowst=1;
        irowen=1;
        if isfield(par,'vox_dim_ras')
          vol=prod(par.vox_dim_ras);
        elseif isfield(par,'vox_volume')
          vol=par.vox_volume;
        else
          vol=1;
        end
      end

      tramp=1.0;

      if 0 && ~isempty(header)
        te=round(header.image.te/1000);
        nunfil=header.rdb_hdr.frame_size; % size(data,3) includes zero-padding, which we don't want.
        deltat=1/header.rdb_hdr.spectral_width;
        hzppm=header.rdb_hdr.ps_mps_freq/1000000;
      end

      te=self.MRS_struct.p.TE;
      nunfil=self.MRS_struct.p.npoints; % do not include zero-fill for LCModel
      deltat=1/self.MRS_struct.p.sw;
      hzppm=self.MRS_struct.p.LarmorFreq;


      flag_diff=strcmp(spec,'diff');
      flag_edit=strcmp(spec,'on')||flag_diff;

      % Determine which basis set to use... {{{
      configured_basis=MRS_interconnect_config.get('basis');
      basis_candidates=configured_basis.filter('TE',te,'edit',flag_edit,'diff',flag_diff);
      basis_candidates.show();

      if isempty(basis_candidates)
        self.log(sprintf('No suitable basis set configured (in %s) for TE=%d, edit=%d, diff=%d',formatting.editor_link(which('MRS_interconnect_config'),65,'MRS_interconnect_config.m'),te,flag_edit,flag_diff));
        all_bases=self.find_basis();
        disp('These are the currently-configured options:');
        configured_basis.show();
        if size(all_bases,1)==0
          disp(['...and no more basis sets could be found. Check that the path_lcmodel_basis option is set correctly in MRS_interconnect_config.m (current value: ' MRS_interconnect_config.get('path_lcmodel_basis') ')']);
          self.log(['Basis set path may be incorrect; check path_lcmodel_basis in ' formatting.editor_link(which('MRS_interconnect_config'),60,'MRS_interconnect_config.m') ' (current value: ' MRS_interconnect_config.get('path_lcmodel_basis') ')']);
        else
          disp('...and these are the options found in your LCModel basis-sets folder')
          self.find_basis().show();

          if flag_diff
            % search on TE, 'MEGA', vendor (substring)
            basis_candidates=self.find_basis('ECHOT',te,'keyword','MEGA','keyword',self.MRS_struct.p.vendor(1:min(5,end)));
            if size(basis_candidates,1)==0
              % no matches? drop the vendor constraint
              basis_candidates=self.find_basis('ECHOT',te,'keyword','MEGA');
            end
          else
            basis_candidates=self.find_basis('ECHOT',te,'SEQ','PRESS','keyword',self.MRS_struct.p.vendor(1:min(5,end)));
            if size(basis_candidates,1)==0
              % no matches? drop the vendor constraint
              basis_candidates=self.find_basis('ECHOT',te,'SEQ','PRESS');
            end
          end
        end
        if size(basis_candidates,1)==0
          self.log('Automatic search for suitable basis set also failed.');
        else
          self.log(['Automatic search for suitable basis set found this: ' basis_candidates(1,'filename')]);
        end
      else
        basis_candidate_vendorspecific=basis_candidates.filter('vendor',regexprep(self.MRS_struct.p.vendor,'_.*$',''),'missing','false');
        if isempty(basis_candidate_vendorspecific)
          self.log('No vendor-specific basis set identified in your configuration; using generic one')
        else
          basis_candidates=basis_candidate_vendorspecific;
        end
      end

      if size(basis_candidates,1)>0
        chosen_basis=basis_candidates(1,'filename');
        [d,n,e]=fileparts(chosen_basis);
        if length(d)==0
          chosen_basis=fullfile(MRS_interconnect_config.get('path_lcmodel_basis'),chosen_basis);
        end
        if exist(chosen_basis,'file')
          self.log(['Using basis set: ' chosen_basis ]);
        else
          self.log(['Configured basis set ' chosen_basis ' could not be found on this system (not necessarily a problem, if you plan to run the quantification on a separate machine)'],'warn')
        end
      else
        self.log(['The search for a basis set was entirely fruitless. Please check the configuration in ' formatting.editor_link(which('MRS_interconnect_config.m'),65,'MRS_interconnect_config.m') ' (cfg.basis and/or cfg.path_lcmodel_basis items)'],'warn');
        chosen_basis='UNKNOWN';
      end

      filename_variant=self.get_filename_variant('vox',vox,'target',target,'spec',spec);

      for doref=0:1 % first iteration writes the water reference, second iteration writes the raw data. {{{
        if doref
          if isempty(ref)
            if tramp==1.0
              warning('Using default TRAMP value with no water reference : non-ratio concentration estimates will be oddly scaled.');
            end
            continue;
          end;
          wdata=ref;
          ofn=self.get_filename([ filename_variant '.REF' ]);
          self.log([ 'Saving water reference data : ' ofn]);
        else
          ofn=self.get_filename([ filename_variant '.RAW' ]);
          self.log([ 'Saving raw metabolite data  : ' ofn]);
          wdata=data;
        end % doref
        f=fopen(ofn,'wt');

        [x,y,fname]=fileparts(self.get_filename());

        fprintf(f,' $NMID\n');
        fprintf(f,' ID=''%s''\n', [ fname filename_variant] );
        fprintf(f,' FMTDAT=''(4E20.5)''\n');
        fprintf(f,' VOLUME= %f\n', vol); % in consistent units (eg, ml)
        fprintf(f,' TRAMP= %f\n', tramp);
        fprintf(f,' $END\n');

        if is_csi and size(data,1)==nr and size(data,2)==nc % multi-voxel (CSI) data
          for rr=1:nr
            for cc=1:nc
              for t=1:2:nunfil
                  fprintf(f, '%20.5E%20.5E%20.5E%20.5E\n', real(wdata(rr,cc,t)), imag(wdata(rr,cc,t)), real(wdata(rr,cc,t+1)), imag(wdata(rr,cc,t+1)));
              end
            end
          end
        else % not csi...
          if multifid
            % no spatial info, but multiple fids. could be timeseries or different windows....
            for ic=1:nc
              for t=1:2:nunfil
                fprintf(f, '%20.5E%20.5E%20.5E%20.5E\n', real(wdata(ic,t)), imag(wdata(ic,t)), real(wdata(ic,t+1)), imag(wdata(ic,t+1)));
              end
            end
          else
            % no spatial info, just a single fid
            for t=1:2:nunfil
              fprintf(f, '%20.5E%20.5E%20.5E%20.5E\n', real(wdata(t)), imag(wdata(t)), real(wdata(t+1)), imag(wdata(t+1)));
            end
          end
        end

        fclose(f);

      end % doref }}}

      basis=chosen_basis;

      ppmst=4.0;
      ppmend=0.2;

      if ~isempty(strfind(target,'GABA'))
        ppmst=4.2;
        if flag_diff
          control_extra.sptype='''mega-press-3''';
        end
      elseif ~isempty(strfind(target,'GSH'))
        ppmst=3.6;
        if flag_diff
          control_extra.sptype='''mega-press-3''';
        end
      elseif ~isempty(strfind(target,'Lac'))

      else
        ppmst=4.0;
      end
      
      % per suggestions in LCModel manual...
      if strcmpi(self.MRS_struct.p.vendor,'GE')||strncmpi(self.MRS_struct.p.vendor,'Siemens',7)
        sddegp=1;
        degppm=0.0; % default
      else
        % philips
        sddegp=2;
        degppm=2.5;
      end

      control_extra.DOECC='F';

      if ~isempty(ref)
        control_extra.FILH2O=sprintf('"%s"',self.get_filename([filename_variant '.REF']));
        control_extra.DOWS='T';
        if ~flag_diff
          control_extra.DOECC='T';
        end
      else
        control_extra.DOWS='F';
      end

      cfn=self.get_filename([ filename_variant '.control']);
      self.log(['Saving LCModel control file : ' cfn]);

      % default control values:
      control=struct(...
        'DELTAT',  sprintf('%2.3e',deltat),                                  ...
        'ECHOT',   sprintf('%d',te),                                         ...
        'HZPPPM',  sprintf('%2.5e', hzppm),                                  ...
        'NUNFIL',  sprintf('%d', nunfil),                                    ...
        'PPMST',   sprintf('%0.1f', ppmst),                                  ...
        'PPMEND',  sprintf('%0.1f', ppmend),                                 ...
        'SDDEGP',  '1.',                                                     ...
        'SDDEGZ',  '3.',                                                     ...
        'IAVERG',  '0',                                                      ...
        'ISLICE',  '1',                                                      ...
        'ICOLST',  sprintf('%d', icolst),                                    ...
        'ICOLEN',  sprintf('%d', icolen),                                    ...
        'IROWST',  sprintf('%d', irowst),                                    ...
        'IROWEN',  sprintf('%d', irowen),                                    ...
        'NDCOLS',  sprintf('%d', nc),                                        ...
        'NDROWS',  sprintf('%d', nr),                                        ...
        'NDSLIC',  '1',                                                      ...
        'FILBAS',  sprintf('"%s"',basis),                                    ...
        'FILRAW', sprintf('"%s"', self.get_filename([filename_variant '.RAW'])),  ...
        'FILPS', sprintf('"%s"',self.get_filename([filename_variant '.ps'])),     ...
        'FILTAB', sprintf('"%s"',self.get_filename([filename_variant '.table'])), ...
        'LTABLE', '7',                                                       ...                
        'FILCSV', sprintf('"%s"',self.get_filename([filename_variant '.csv'])),   ...
        'LCSV', '11',                                                        ...                
        'FILCOO', sprintf('"%s"',self.get_filename([filename_variant '.coo'])),   ...
        'LCOORD', '9',                                                       ...
        'TITLE', strtrim(sprintf('"%s %s %s %s"', vox, target, spec, self.get_provenance_string())) ...
        );
  
      % next, merge in any extra control elements
      fns=fieldnames(control_extra);
      for i=1:length(fns)
        k=upper(strtrim(fns{i}));
        control.(k)=control_extra.(fns{i});
      end

      % finally, merge/overwrite with any user-specified elements
      if ~isempty(control_override)
        if isstruct(control_override) % merge values from control_override struct
          fns=fieldnames(control_override);
          for i=1:length(fns)
            k=upper(strtrim(fns{i}));
            v=control_override.(fns{i});
            if isnumeric(v)
              v=sprintf('%d',v);
            end
            control.(k)=v;
          end
        elseif iscell(control_override) % read key/value pairs from cell array
          for i=2:2:length(control_override)
            k=upper(strtrim(control_override{i-1}));
            v=control_override{i};
            if isnumeric(v)
              v=sprintf('%d',v);
            end
            control.(k)=v;
          end
        else
          error('control parameter must be either a struct or cell array with key/value pairs')
        end

      end

      txt={' $LCMODL'};
      fns=fieldnames(control);
      for i=1:length(fns)
        if ~isempty(control.(fns{i}))
          % only write non-empty fields (the allows users to remove default entries by setting an empty field in the override struct)
          txt{end+1}=sprintf(' %s= %s',fns{i},control.(fns{i}));
        end
      end

      f=fopen(cfn,'wt');
      fwrite(f,sprintf('%s\n',txt{:}));
      fclose(f);

      disp(self.get_filename([filename_variant '.control']));

    end % function }}}

  end % }}}

  methods(Static)

    function basis=find_basis(varargin)
      % FIND_BASIS attempt to select appropriate basis set for this data
      %
      % If nothing suitable is found in MRS_interconnect_config, the basis-sets folder is scanned
      % for candidates.
      %
      % See Also: MRS_interconnect_config,

      global basis_set_cache;

      src=MRS_interconnect_config.get('path_lcmodel_basis');
      basis=[];
      %basis_set_cache=[];

      if ~exist('basis_set_cache') || isempty(basis_set_cache)
        files=dir(src);
        all_basis=datatable_ARC();
        fprintf('Scanning basis sets in %s...', src);
        for i=1:length(files)
          fprintf('.');
          fn=files(i).name;
          if length(fn)>6 && strcmp(fn(end-5:end),'.basis')
            basis=MRS_interconnect_LCModel.read_parameters(fullfile(src,fn));

            row=ordered_map_ARC({'fullpath','filename'},{fullfile(src,fn),fn});
            row=[row; basis.get_row('section','SEQPAR'); basis.get_row('section','BASIS1')];
            if basis.isKey('METABO')
              metabs=basis.unique('METABO');
              for k=1:length(metabs)
                row(metabs{k})=metabs{k};
              end;
            end
            all_basis.append(row);
          end
        end
        basis_set_cache=all_basis;
      else
        all_basis=basis_set_cache;
      end;
      basis=all_basis;
      while ~isempty(varargin) % {{{
        switch lower(varargin{1})
          case 'keyword'
            basis=basis.filter('filename',varargin{2},'SEQ',varargin{2},'IDBASI',varargin{2},'operator','or','match','substring','missing','true');
          otherwise
            basis=basis.filter(varargin{1},varargin{2});
        end
        varargin(1:2)=[];
      end % while }}}
    end

    function res=check_support(op,silent)
      % CHECK_SUPPORT reports capabilities of this module, in the current configuration.
      %
      % See Also: MRS_interconnect_base.check_support

      if (nargin<2) silent=0; end;
      if (nargin<1) op=[]; end;
      res=struct('read',1,'write',1,'import',1,'run',0,'intermediate',0);
      % check dependencies
      if isunix || ismac
        [stat,txt]=system([MRS_interconnect_config.get('path_lcmodel') ' < /dev/null >/dev/null 2>&1']);
        if stat==127 
          if (~silent) warning('LCModel binary could not be found.'); end;
        else
          res.run=1;
        end;
      else
        if (~silent) warning('LCModel not available on Windows system.'); end;
      end;

      if ~isempty(op)
        res=res.(op)
      end;
    end

    function res=can_import(d)
      % check whether this module is able to import data in the supplied format
      res=0;
    end

    function out=read_coords_file(fn) % {{{
      % READ_COORDS_FILE reads LCModel coords data from the specified file
      %
      % See also: MRS_interconnect_LCModel.read_coords

      fh=fopen(fn);
      ppm=[];
      fit=[];
      data=[];
      baseline=[];

      mode='nothing';
      while (~feof(fh))
        ln=fgets(fh);
        if (~isstr(ln)) break; end;

        vals=regexp(ln,'[ ]+([-+.E0-9]+)','tokens');
        ln=strtrim(ln);

        x=regexp(ln,'[ ]*(?<pts>[0-9]+)[ ]*points on ppm-axis = NY$');
        if (~isempty(x))
          mode='ppm';
          continue;
        end;

        x=regexp(ln,'[ ]*NY phased data points follow$');
        if (~isempty(x))
          mode='data';
          continue;
        end;

        x=regexp(ln,'[ ]*NY points of the fit to the data follow$');
        if (~isempty(x))
          mode='fit';
          continue;
        end;

        x=regexp(ln,'[ ]*NY background values follow$');
        if (~isempty(x))
          mode='baseline';
          continue;
        end;

        x=regexp(ln,'[ ]*lines in following.*$');
        if (~isempty(x))
          mode='nothing';
          continue;
        end;

        if max(size(vals))>2
          dvals=[];
          for x=vals
            dvals(end+1)=str2double(x{:});
          end
          switch(mode)
            case 'ppm'
              ppm(end+1:end+length(dvals))=dvals;
            case 'data'
              data(end+1:end+length(dvals))=dvals;
            case 'fit'
              fit(end+1:end+length(dvals))=dvals;
            case 'baseline'
              baseline(end+1:end+length(dvals))=dvals;
          end
        else
          mode='nothing';
        end

      end; % while
      fclose(fh);
      out=struct('data',data,'fit',fit,'ppm',ppm,'baseline',baseline);
      % }}}
    end


    function out=read_parameters(fn, varargin) % {{{
      % read_parameters : parse LCModel-style KEY=val pairs from text file (eg: LCModel .control, .basis, .RAW)

      out=datatable_ARC();

      if (~exist(fn,'file'))
        % read_parameters is a bit more generic than the other read functions, so we need to pass this error up for handling depending on context
        throw(MException('MRS_interconnect:FileNotFound',sprintf('Parameter file not found: %s', fn)));
      end;

      fh=fopen(fn);

      if (fh<0) 
        error('could not open file.');
        return; 
      end;

      in_section=[];

      while ~feof(fh);
        str=fgets(fh);
        if (~isstr(str)) break; end;

        % quick check whether this line is of interest -- if not, we can save on the heavy regexp processing
        if (isempty(strfind(str,'=')) && isempty(strfind(str,'$'))) continue; end;

        ln=strtrim(str); %fgets(fh));
        section_marker=regexp(ln,'[ ]*\$(?<marker>[a-zA-Z0-9]+)[ ]*$','names');
        if ~isempty(section_marker)
          if strcmpi(section_marker.marker,'END')
            if ~isempty(in_section)
              if length(keys)>1
                out.append(keys,vals);
              end
              in_section=[];
            end
          else
            in_section=section_marker.marker;
            keys={'section'};
            vals={in_section};
          end;
        end;
        if isempty(in_section)
          % no sense processing further: these values have nowhere to go
          continue;
        end;
        x=regexp(ln,'[ ]*(?<key>[a-zA-Z0-9]+)[ ]*=[ '']*(?<value>[^'']*)['', ]*$','names');
        if (~isempty(x))
          x=x(1);
          if length(x.value)==0
            str=fgets(fh);
            if (~isstr(str)) break; end;
            ln2=strtrim(str);
            xx=regexp(ln2,'[ ]*''(?<value>[^'']*)['', ]*$','names');
            if ~isempty(xx)
              x.value=xx(1).value;
            end;
          end;
          x.value=regexprep(x.value,'[., ]+$',''); % remove trailing junk
          if strcmp(x.value,'T')
            x.value=1;
          elseif strcmp(x.value,'F')
            x.value=0;
          else
            dv=str2double(x.value);
            if (~isnan(dv)) x.value=dv; end;
          end;
          key=x.key;
          if (length(key)<60)
            try
              if in_section
                keys{end+1}=key;
                vals{end+1}=x.value;
              end
            catch Ex
             rethrow(Ex);
             continue;
            end;
          end
        end
      end
      fclose(fh);
      % do we have any parameters left over from the final section? (.control files don't explicitly terminate all their blocks)
      if ~isempty(in_section)
        if length(keys)>1
          out.append(keys,vals);
        end
        in_section=[];
      end
    end % read_parameters }}}

  end

end
