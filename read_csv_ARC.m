function out=read_csv(fn);
  % READ_CSV(fn) import numeric data from CSV file (including header row) into a keyed map
  %
  % Alexander R. Craven, University of Bergen, 2018-10-17

  % (note, the keyed map is used rather than a structure, since the latter has severe limitations in suitable characters for field names)

  if ~exist(fn,'file')
    error(['The specified file does not exist: ' fn]);
  end

  fh=fopen(fn); % ? may need to handle encoding/byte order issues here, on some platforms?

  values={};
  in_header=1;
  is_numeric=1;
  line_num=0;

  while ~feof(fh);

    str=strtrim(fgets(fh));

    if (length(str)==0) continue; end; % skip blank lines

    line_num=line_num+1;
    in_header=(line_num==1);

    if (regexp(str,'^("[^"]*"[ ]*,[ ]*)+("[^"]*")(,)?$')) % deal with quotes, if we must...
      b1=regexp(str,'"([^"])*"[ ]*(?:,|$)','tokens'); % split quoted parts by delimiter
      vals={};
      for i=1:length(b1)
        b2=char(b1{i});
        b2=regexprep(b2,'(^")|("[ ]+[,]?)',''); % strip out leading quotes, and trailing quotes, whitespace, delimiters
        if isempty(b2)
          b2=[];
        elseif regexp(b2,'^[+-]?[0-9]+,[0-9]+([eE][+-]?[0-9]+)?') % NOrwegian locale: comma used as decimal separator. Switch back to '.' so as to not confuse str2double
          b2=strrep(b2,',','.');
        end
        %if ~isempty(b2) && ~isempty(regexp(b2,'^[+-]?[0-9]+(\.[0-9]+)?([eE][+-]?[0-9]+)?'))
        %  b2=str2double(b2)
        %end
        vals{end+1}=b2;
      end
    else % no quotes, just split by delimiter and perhaps arbitrary whitespace
      vals=regexp(str,'[ ]*,[ ]*','split');
    end

    if (in_header)
      header=vals;
    else
      if is_numeric
        % try to parse the values as numbers...
        try
          nvals=str2double(vals);
          if (any(isnan(nvals)))
            is_numeric=0;
          end
        catch Ex
          is_numeric=0;
        end
        if is_numeric
          vals=nvals;
        end
      end
      values{end+1}=vals;
    end
  end

  out=datatable_ARC(header,values);

  fclose(fh);
end
