classdef MRS_interconnect_Gannet < MRS_interconnect_base
  % MRS_interconnect_Gannet import/quantify with Gannet
  %
  % This module provides a wrapper to GannetLoad and GannetFit functionality.
  %
  % Alexander R. Craven, University of Bergen, 2018-10-17

  properties (Access=private)
    gannet_ver;
  end

  methods % {{{

    function self=MRS_interconnect_Gannet(d,varargin)
      self=self@MRS_interconnect_base(d,varargin{:});
      %self.import(d)
    end

    function res=run(self)
      self.log_provenance('Quant');
      self.log('Quantify with GannetFit');
      self.MRS_struct=GannetFit(self.MRS_struct);
      res=self.MRS_struct;
    end

    function res=wrap(self)
      % export data, run the analysis and return the results
      self.log('Begin wrap: run')
      res=self.run();
      self.report();
    end

    function res=import(self,d)
      % import data in the format specific to this application
      if isstruct(d) && isfield(d,'metabfile')
        self.gannet_ver=3;
        self.MRS_struct=d;
        disp('Imported 3.0 MRS_struct');
      elseif isstruct(d) && isfield(d,'GABAfile')
        self.gannet_ver=2;
        self.MRS_struct=d; % ? we should rather convert this to a 3.0-style structure
      elseif ischar(d) && exist(d,'file') && exist('GannetLoad')
        % Load a single file with GannetLoad -- check if we have an associated water reference:
        paired_water_reference=MRS_interconnect_base.find_paired_reference(d);

        % Load data with Gannet
        if isempty(paired_water_reference)
          self.log(sprintf('Import %s with GannetLoad',d));
          self.MRS_struct=GannetLoad({d});
        else
          self.log(sprintf('Import %s with GannetLoad, using water ref %s',d,paired_water_reference));
          self.MRS_struct=GannetLoad({d},{paired_water_reference});
        end
      elseif ~exist('GannetLoad')
        error('GannetLoad function is not available! Please ensure that Gannet has been added to your matlab path.');
      else
        error('MRS_interconnect_Gannet::import can not handle this data type');
      end
    end;

  end % }}}

  methods(Static)

    function res=check_support(op,silent);
      % CHECK_SUPPORT reports capabilities of this module, in the current configuration.
      %
      % See Also: MRS_interconnect_base.check_support

      if (nargin<2) silent=0; end;
      if (nargin<1) op=[]; end;

      res=struct('read',0,'write',0,'import',0,'run',0,'intermediate',0);

      if ~exist('GannetPreInitialise')
        if (~silent) warning('Gannet could not be found.'); end;
      else
        res=struct('read',1,'write',1,'import',1,'run',1,'intermediate',1);
      end;

      if ~isempty(op)
        res=res.(op)
      end
    end

    function res=can_import(d)
      % check whether this module is able to import data in the supplied format
      % MRS_interconnect_Gannet can handle Gannet 3.0 and older 2.x-style structures

      if isstruct(d) && (isfield(d,'metabfile')||isfield(d,'GABAfile'))
        res=1;
      elseif ischar(d) && (exist(d,'file')) % this is a bit optimistic...
        res=1;
      else
        res=0;
      end;

    end

  end

end
