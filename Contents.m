% MRS_INTERCONNECT module to facilitate communication between different MRS packages
% Version 1 12-Oct-2018
%
% Files
%
%   MRS_interconnect_base                - MRS_interconnect_base : base class for application-specific MRS_interconnect modules
%   MRS_interconnect_config              - MRS_interconnect_config : global configuration for MRS_interconnect modules
%   MRS_interconnect_FIDA                - MRS_interconnect_FIDA : Import/Output and Preprocessing using FID-A functions
%   MRS_interconnect_Gannet              - MRS_interconnect_Gannet import/quantify with Gannet
%   MRS_interconnect_LCModel             - MRS_interconnect_LCModel : LCModel support module
%   MRS_interconnect_LCModel_diagnostics - MRS_interconnect_LCModel_diagnostics : support function for MRS_interconnect_LCModel: lookup diagnostic error code
%   MRS_interconnect_tarquin             - MRS_interconnect_tarquin
%   MRS_plot                             - MRS_plot support functions for plotting MRS data
%   formatting                           - formatting : a collection of output-formatting functions.
%   datatable_ARC                        - datatable_ARC   stores multiple rows of keyed data, in an orderly structure
%   ordered_map_ARC                      - ordered_map_ARC stores key/value pairs in an ordered structure
%   get_kwarg                            - gets the value associated with a keyword argument, from a list of key/value pairs in varargin
%   read_csv_ARC                         - READ_CSV(fn) import numeric data from CSV file (including header row) into a keyed map
%   statusbar                            - minimal desktop status-bar setting function
%
% Examples
%   See the EXAMPLES folder for suggested usage.
%
% Alex Craven (University of Bergen), 2018-08
