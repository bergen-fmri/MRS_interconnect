# MRS\_interconnect

Matlab module to facilitate communication between various different MRS processing packages.

2018-10-17, Alexander R. Craven, Department of Biological and Medical Psychology, University of Bergen

This is experimental software; it is distributed in the hope that it may be useful, but with ABSOLUTELY NO WARRANTY; for details see the [LICENSE](LICENSE) file.

 - This software is available from <https://git.app.uib.no/fmri/spectro/MRS_interconnect>
 - University of Bergen / Haukeland users may reach the latest experimental release (with some additional functionality) here: <https://git.app.uib.no/fmri/spectro/MRS_interconnect>

# Getting Started

The [examples](examples/) folder contains a series of examples showing basic usage patterns, starting relatively simple and working up to a fully automated pipeline. Of particular interest,
 - [ex02\_WrapLCModel](examples/ex02_WrapLCModel.m) demonstrates processing GannetLoad-ed data with LCmodel
 - [ex07\_CompareQuantificationAlgorithms](examples/ex07_CompareQuantificationAlgorithms.m) demonstrates serial quantification in Gannet, LCModel and Tarquin from the same preprocessing.
 - [ex08\_BatchProcessing](examples/ex08_BatchProcessing.m) suggests a automated batch processing pipeline

# Known issues

- There appear to be some inconsistencies with data scaling between different preprocessors; therefore, water-referenced concentrations may be confusing. Creatine-scaled estimates should be unaffected.
- Current (2018-09) release of FID-A uses somewhat older import routines, which do not handle the diversity of megapress implementations as well as the current Gannet routines.
  - A handful of other issues affecting some FID-A releases are detected and reported by the example code, with suggested fixes.

# Dependencies

- Gannet 3.0 [Bergen local](https://git.app.uib.no/fmri/spectro/Gannet3.0/) or [public upsteam](https://github.com/richardedden/Gannet3.0/)
- FID-A [Bergen local](https://git.app.uib.no/fmri/spectro/FID-A) or [public upstream](https://github.com/CIC-methods/FID-A)
